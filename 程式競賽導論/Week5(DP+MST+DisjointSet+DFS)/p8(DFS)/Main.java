import java.util.Arrays;
import java.util.Scanner;

class Main {
	static int city;
	static int road;
	static int[] parent;
	static int[][] matrix;
	static int[] path;
	static boolean hasReachedGoal;

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int flag = 0;
		while(input.hasNext() && flag < 2) {
			if(flag == 1) {
				System.out.println();
			}
			flag = 1;
			
			city = input.nextInt();
			road = input.nextInt();
			Edge[] edge = new Edge[road];
			for(int i=0; i<road; i++) {
				edge[i] = new Edge();
				edge[i].v1 = input.nextInt();
				edge[i].v2 = input.nextInt();
				edge[i].danger = input.nextInt();
			}
			kruskal(edge);
			
			int query = input.nextInt();
			path = new int[city+2];
			for(int i=0; i<query; i++) {
				int v1 = input.nextInt();
				int v2 = input.nextInt();
				path[1] = v1;
				hasReachedGoal = false;
				DFS(v1, v2, 2);
				int maxDanger = 0;
				for(int j=1; path[j]!=v2; j++) {
					maxDanger = Math.max(maxDanger, matrix[path[j]][path[j+1]]);
				}
				System.out.println(maxDanger);
			}
		}
		
		input.close();
	}
	
	static int find(int x) {
		int leaf = x;
		while(parent[x] > 0) {
			x = parent[x];
		}
		int root = x;
		while(parent[leaf] != root && parent[leaf] > 0) {
			int next = parent[leaf];
			parent[leaf] = root;
			leaf = next;
		}
		return root;
	}
	
	static void kruskal(Edge edge[]) {
		Arrays.sort(edge);
		parent = new int[city+1];
		matrix = new int[city+1][city+1];
		int count = 0;
		Arrays.fill(parent, -1);
		for(int i=1; i<=city; i++) Arrays.fill(matrix[i], 0);
		
		for(int i=0; count<city-1;i++) {
			if(find(edge[i].v1) != find(edge[i].v2)) {
				if(parent[find(edge[i].v1)] >= parent[find(edge[i].v2)]) {
					parent[find(edge[i].v2)] += parent[find(edge[i].v1)];
					parent[find(edge[i].v1)] = find(edge[i].v2);
				}
				else {
					parent[find(edge[i].v1)] += parent[find(edge[i].v2)];
					parent[find(edge[i].v2)] = find(edge[i].v1);
				}
				matrix[edge[i].v1][edge[i].v2] = edge[i].danger;
				matrix[edge[i].v2][edge[i].v1] = edge[i].danger;
				count++;
			}
		}
	}
	
	static void DFS(int v1, int v2, int layer) {
		for(int i=1; i<=city; i++) {
			if(hasReachedGoal) return;
			if(matrix[v1][i] > 0) {
				boolean duplicated = false;
				for(int j=1; j<layer; j++) {
					if(path[j] == i) {
						duplicated = true;
						break;
					}
				}
				if(duplicated) continue;
				if(i == v2) {
					hasReachedGoal = true;
					path[layer] = i;
					return;
				}
				path[layer] = i;
				DFS(i, v2, layer+1);
			}
		}
	}
}

class Edge implements Comparable<Edge>{
	int v1;
	int v2;
	int danger;
	
	@Override
	public int compareTo(Edge o) {
		if(this.danger > o.danger) return 1;
		else if(this.danger < o.danger) return -1;
		return 0;
	}
}