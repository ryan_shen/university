import java.util.*;
class Main {
	static int city;
	static int road;
	static int[] parent;
	static int[] dangerToParent;

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int flag = 0;
		while(input.hasNext() && flag < 2) {
			if(flag == 1) {
				System.out.println();
				//input.nextLine();
			}
			flag = 1;
			city = input.nextInt();
			road = input.nextInt();
			Edge[] edge = new Edge[road];
			for(int i=0; i<road; i++) {
				edge[i] = new Edge();
				edge[i].v1 = input.nextInt();
				edge[i].v2 = input.nextInt();
				edge[i].danger = input.nextInt();
			}
			Arrays.sort(edge);
			parent = new int[city+1];
			dangerToParent = new int[city+1];
			int count = 0;
			Arrays.fill(parent, -1);
			Arrays.fill(dangerToParent, 0);
			for(int i=0; count<city-1;i++) {
				if(find(edge[i].v1) != find(edge[i].v2)) {
					if(parent[edge[i].v1] > 0) {
						reverseTo(edge[i].v1, edge[i].v2, edge[i].danger);
					}
					else if(parent[edge[i].v2] > 0) {
						reverseTo(edge[i].v2, edge[i].v1, edge[i].danger);
					}
					else {
						parent[edge[i].v1] = edge[i].v2;
						dangerToParent[edge[i].v1] = edge[i].danger;
					}
					count++;
				}
			}
			int query = input.nextInt();
			for(int i=0; i<query; i++) {
				int v1 = input.nextInt();
				int v2 = input.nextInt();
				int depthV1 = 0;
				int depthV2 = 0;
				int tempV = v1;
				while(parent[tempV] > 0) {
					tempV = parent[tempV];
					depthV1++;
				}
				tempV = v2;
				while(parent[tempV] > 0) {
					tempV = parent[tempV];
					depthV2++;
				}
				int maxDanger = 0;
				while(v1 != v2) {
					if(depthV1 >= depthV2) {
						depthV1--;
						maxDanger = Math.max(maxDanger, dangerToParent[v1]);
						v1 = parent[v1];
					}
					else {
						depthV2--;
						maxDanger = Math.max(maxDanger, dangerToParent[v2]);
						v2 = parent[v2];
					}
				}
				System.out.println(maxDanger);
			}	
			
		}
		input.close();
	}
	
	static int find(int x) {
		while(parent[x] > 0) {
			x = parent[x];
		}
		return x;
	}
	
	static void reverseTo(int v1, int v2, int danger) {
		int flag = 0;
		while(true) {
			int v3 = parent[v1];
			int dangerToV3 = dangerToParent[v1];
			parent[v1] = v2;
			dangerToParent[v1] = danger;
			if(flag == 1) break;
			if(parent[v3] < 0) flag = 1;
			v2 = v1;
			v1 = v3;
			danger = dangerToV3;
		}
	}
}

class Edge implements Comparable<Edge>{
	int v1;
	int v2;
	int danger;
	
	@Override
	public int compareTo(Edge o) {
		if(this.danger > o.danger) return 1;
		else if(this.danger < o.danger) return -1;
		return 0;
	}
}