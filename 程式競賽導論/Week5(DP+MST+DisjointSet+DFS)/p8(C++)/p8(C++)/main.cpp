#include <iostream>
#include <vector>
#include <algorithm>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

int city, road;
int parent[50001];
int path[10000];
int danger[10000];
int pathAmount;
bool passed;
bool visited[50001];

class Edge{
public:
    Edge(){};
    Edge(int n, int danger): n(n), danger(danger){};
    int v1;
    int v2;
    int danger;
    int n;
};

vector<Edge> vec[50001];

int find(int x)
{
    int leaf = x;
    while(parent[x] > 0){
        x = parent[x];
    }
    int next;
    while(parent[leaf] != x && leaf != x){
        next = parent[leaf];
        parent[leaf] = x;
        leaf = next;
    }
    return x;
}

void DFS(int v1, int v2, int layer)
{
    vector<Edge>::iterator it;
    for(it=vec[v1].begin(); it!=vec[v1].end(); it++){
        if(passed) return;
        if(visited[it->n]) continue;
        path[layer] = it->n;
        danger[layer] = it->danger;
        if(it->n == v2){
            passed = true;
            pathAmount = layer;
            return;
        }
        visited[it->n] = true;
        DFS(it->n, v2, layer+1);
        visited[it->n] = false;
    }

}

bool cmp(Edge a, Edge b)
{
    return a.danger < b.danger;
}

int main(int argc, const char * argv[]) {
    int test = 0;
    while(scanf("%d%d", &city, &road) != EOF){
        if(test++) printf("\n");
        Edge edge[road];
        for(int i=0; i<road; i++){
            scanf("%d%d%d", &edge[i].v1, &edge[i].v2, &edge[i].danger);
        }
        sort(edge, edge+road, cmp);
        int count = 0;
        memset(parent, -1, (city+1)*4);
        for(int i=1; i<=city; i++) vec[i].clear();
        for(int i=0; count<city-1; i++){
            if(find(edge[i].v1) != find(edge[i].v2)){
                if(parent[find(edge[i].v1)] >= parent[find(edge[i].v2)]){
                    parent[find(edge[i].v2)] += parent[find(edge[i].v1)];
                    parent[find(edge[i].v1)] = find(edge[i].v2);
                }
                else{
                    parent[find(edge[i].v1)] += parent[find(edge[i].v2)];
                    parent[find(edge[i].v2)] = find(edge[i].v1);
                }
                vec[edge[i].v1].push_back(Edge(edge[i].v2, edge[i].danger));
                vec[edge[i].v2].push_back(Edge(edge[i].v1, edge[i].danger));
                count++;
            }
        }
        int query;
        scanf("%d", &query);
        for(int i=0; i<query; i++){
            int v1, v2;
            scanf("%d%d", &v1, &v2);
            path[1] = v1;
            passed = false;
            visited[v1] = true;
            DFS(v1, v2, 2);
            visited[v1] = false;
            int maxValue = 0;
            for(int j=2; j<=pathAmount; j++)
                maxValue = max(maxValue, danger[j]);
            printf("%d\n", maxValue);
        }
    }
}
