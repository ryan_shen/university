#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>

using namespace std;

class Gate{
public:
    double left;
    int y;
    double right;
};

Gate gate[100005];
int ski[1000005];
int width;
int nGate;
double vh;

bool isPromising(int mid) {
    double left = gate[nGate-1].left;
    double right = gate[nGate-1].right;
    for(int j=nGate-2; j>=0; j--) {
        double m = vh / ski[mid];
        m *= (gate[j+1].y - gate[j].y);
        left -= m;
        right += m;
        left = max(left, gate[j].left);
        right = min(right, gate[j].right);
        if(left - right > 1e-10) return false;
    }
    return true;
}

int main(int argc, const char * argv[]) {
    
    int test;
    scanf("%d", &test);
    while(test--) {
        scanf("%d%lf%d", &width, &vh, &nGate);
        for(int i=0; i<nGate; i++) {
            scanf("%lf%d", &gate[i].left, &gate[i].y);
            gate[i].right = gate[i].left + width;
        }
        int nSki;
        scanf("%d", &nSki);
        for(int i=0; i<nSki; i++) {
            scanf("%d", &ski[i]);
        }
        
        sort(ski,ski+nSki);
        int start = 0;
        int end = nSki;
        int mid;
        if(!isPromising(0))
            printf("IMPOSSIBLE\n");
        else {
            while(start != end - 1) {
                mid = (start + end) / 2;
                if(isPromising(mid)) start = mid;
                else end = mid;
            }
            printf("%d\n", ski[start]);
        }
    }
}
        

    


