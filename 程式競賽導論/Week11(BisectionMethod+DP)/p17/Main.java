import java.util.*;
public class Main {
	static Gate[] gate;
	static int[] ski;
	static int nGate;
	static double vh;
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int test = input.nextInt();
		while(test-- > 0) {
			int width = input.nextInt();
			vh = input.nextDouble();
			nGate = input.nextInt();
			gate = new Gate[nGate];
			for(int i=0; i<nGate; i++) {
				gate[i] = new Gate();
				gate[i].left = input.nextDouble();
				gate[i].y = input.nextInt();
				gate[i].right = gate[i].left + width;
			}
			int nSki = input.nextInt();
			ski = new int[nSki];
			for(int i=0; i<nSki; i++) {
				ski[i] = input.nextInt();
			}
			
			Arrays.sort(ski);
			int start = 0;
			int end = nSki;
			int mid;
			if(!isPromising(0)) System.out.println("IMPOSSIBLE");
			else {
				while(start != end - 1) {
					mid = (start + end) / 2;
					if(isPromising(mid)) start = mid;
					else end = mid;
				}
				System.out.println(ski[start]);	
			}
		}
	}
	
	public static boolean isPromising(int mid) {
		double left = gate[nGate-1].left;
		double right = gate[nGate-1].right;
		for(int j=nGate-2; j>=0; j--) {
			double m = vh / ski[mid];
			m *= (gate[j+1].y - gate[j].y);
			left -= m;
			right += m;
			left = Math.max(left, gate[j].left);
			right = Math.min(right, gate[j].right);
			if(left > right) return false;
		}
		return true;
	}

}

class Gate{
	double left;
	int y;
	double right;
}
