//
//  main.cpp
//  Prim's Minimum Spanning Tree
//
//  Created by 文文 on 2017/11/7.
//  Copyright © 2017年 文文. All rights reserved.
//

#include <iostream>
#define maxValue 100
using namespace std;

int main(int argc, const char * argv[]) {
    int n;
    cin >> n;
    int w[n+1][n+1];
    for(int i=1; i<=n; i++){
        for(int j=1; j<=n; j++){
            cin >> w[i][j];
        }
    }
    
    int nearest[n+1];       // 對每個點 nearest[i]，距離他最近的點
    int distance[n+1];      // 對每個點 distance[i]，距離他最近的點的距離
    int set[n+1];           // 對每個點 set[i]，是否已包含進新的生成樹
    for(int i=1; i<=n; i++){
        nearest[i] = 1;
        distance[i] = w[1][i];
        set[i] = 0;
    }                       // 假設每個點一開始都離 v1 最近
    
    
    int minPoint,minDist;
    set[1] = 1;
    for(int j=1; j<n; j++){
        minDist = maxValue;
        for(int i=2; i<=n; i++){
            if(set[i] == 1) continue;
            if(distance[i] < minDist){
                minPoint = i;
                minDist = distance[i];
            }
        }                   // 求出此輪中距離生成樹最近的點及其邊長
        set[minPoint] = 1;  // 將該點加入集合
        for(int i=2; i<=n; i++){
            if(set[i] == 1) continue;
            if(w[i][minPoint] < distance[i]){
                distance[i] = w[i][minPoint];
                nearest[i] = minPoint;
            }
        }                   // 對每個其他點，計算其距離剛剛加入的點是否較之前短，若是則更新
    }
    for(int i=1; i<=n; i++){
        cout << nearest[i] << " " << distance[i] << endl;
    }
    return 0;
}
