//
//  main.cpp
//  Floyd's Shortest Path
//
//  Created by Wayne on 2017/11/2.
//  Copyright © 2017年 Wayne. All rights reserved.
//

#include <iostream>
using namespace std;

int main(int argc, const char * argv[]) {
    int n;
    cin >> n;
    int d[n+1][n+1],w[n+1][n+1];
    for(int i=1; i<=n; i++){
        for(int j=1; j<=n; j++){
            cin >> w[i][j];
        }
    }
    
    memcpy(d, w, sizeof(w));
    for(int k=1; k<=n; k++){
        for(int i=1; i<=n; i++){
            for(int j=1; j<=n; j++){
                d[i][j] = min(d[i][j], d[i][k] + d[k][j]);
            }
        }
    }
    
    for(int i=1; i<=n; i++){
        for(int j=1; j<=n; j++){
            cout << d[i][j] << " ";
        }
        cout << endl;
    }
    return 0;
}
