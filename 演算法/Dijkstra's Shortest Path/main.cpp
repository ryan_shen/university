//
//  main.cpp
//  Dijkstra's Shortest Path
//
//  Created by 文文 on 2017/11/4.
//  Copyright © 2017年 文文. All rights reserved.
//

#include <iostream>
#define maxValue 100
using namespace std;

int main(int argc, const char * argv[]) {
    int n,start,end;
    cin >> n;   //輸入頂點數
    int d[n+1],w[n+1][n+1];     // d 為用來記錄最短路徑的陣列
    for(int i=1; i<=n; i++){
        for(int j=1; j<=n; j++){
            cin >> w[i][j]; //輸入路徑圖
        }
    }
    
    cin >> start >> end;    //輸入起點與終點
    int set[n+1];   //為集合，用來包含已算出最短路徑的點
    int minDist = maxValue, minPoint = 0;
    set[start] = 1;
    for(int i=1; i<=n; i++){
        d[i] = w[start][i];
        if(i != start) set[i] = 0;
        if(set[i] == 0 && d[i] < minDist){
            minDist = d[i];
            minPoint = i;
        }
    }   //初始化 d 陣列以及算出第一個距離 start 最近的點
    
    for(int j=2; j<=n; j++){
        set[minPoint] = 1;
        int k = minPoint;
        minDist = maxValue;
        for(int i=1; i<=n; i++){
            d[i] = min(d[i], d[k] + w[k][i]);   // 若 start 到 i 中間經過 k 時有較小值則更新
            if(set[i] == 0 && d[i] < minDist){
                minDist = d[i];
                minPoint = i;
            }
        }
    }   // 重複做 n-1 次刷新 d 陣列的值
    cout << d[end] << endl;
    
    return 0;
}
