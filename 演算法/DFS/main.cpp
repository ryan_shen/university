#include <iostream>

using namespace std;

char arr[100][100];
bool isValid = false;

void func(int y, int x)
{
    if(arr[y][x] == 'x'){
        isValid = true;
        return;
    }
    else if(arr[y][x] != 'o') arr[y][x] = '+';
    if(arr[y-1][x] != '*' && arr[y-1][x] != '+' && arr[y-1][x] != 'o' && !isValid) func(y-1, x);
    if(arr[y][x+1] != '*' && arr[y][x+1] != '+' && arr[y][x+1] != 'o' && !isValid) func(y, x+1);
    if(arr[y+1][x] != '*' && arr[y+1][x] != '+' && arr[y+1][x] != 'o' && !isValid) func(y+1, x);
    if(arr[y][x-1] != '*' && arr[y][x-1] != '+' && arr[y][x-1] != 'o' && !isValid) func(y, x-1);
    if(arr[y][x] != 'o' && !isValid) arr[y][x] = ' ';
}

int main(int argc, const char * argv[]) {
    
    int n; //n x n square matrix
    int startX = 0, startY = 0;
    int endX = 0, endY = 0;
    
    cin >> n; //buffer 會留下 enter 給下一個輸入
    cin.get(); //用來吃掉換行符號
    for(int i=0; i<n; i++) cin.getline(arr[i],100);
    
    for(int i=0; i<n; i++){
        for(int j=0; j<n; j++){
            if(arr[i][j] == 'o'){
                startX = j;
                startY = i;
            }
            else if(arr[i][j] == 'x'){
                endX = j;
                endY = i;
            }
        }
    }
    func(startY, startX);
    for(int i=0; i<n; i++){
        for(int j=0; j<n; j++){
            cout << arr[i][j];
        }
        cout << endl;
    }
    isValid? cout<<"Pass!\n" : cout<<"Fail!\n";
    return 0;
}
