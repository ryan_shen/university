package com.example.dbproject;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    private final static String TAG = "MapsActivity";
    private GoogleMap mMap;

    private static int PERMISSION_REQUEST_CODE = 1;
//private Location mLocation;

    private static final String[] INITIAL_PERMS = {
            android.Manifest.permission.ACCESS_FINE_LOCATION,
            android.Manifest.permission.ACCESS_COARSE_LOCATION
    };

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        if (checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                checkSelfPermission(android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED) {
            //Toast.makeText(this, "BBBB", Toast.LENGTH_LONG).show();

        } else {
            //Toast.makeText(this, "AAAAAA", Toast.LENGTH_LONG).show();
            requestPermissions(INITIAL_PERMS,PERMISSION_REQUEST_CODE);
        }



    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng sydney;
        LocationManager lm = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        //取得目前座標，預設為輔大
        double lng, lat;
        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);// 設定定位資訊由 GPS提供
        if (location!=null){
            lng = location.getLongitude(); // 取得經度
            lat = location.getLatitude(); // 取得緯度
            Log.e("current lat lng", lat + ", " + lng);
            //Toast.makeText(getApplicationContext(),"Yes",Toast.LENGTH_SHORT);
        }
        else{
            lng = 121.434413; // 取得經度
            lat = 25.030518; // 取得緯度
            //Toast.makeText(getApplicationContext(),"No",Toast.LENGTH_SHORT);
        }

        if(getIntent().getExtras() != null) {
            Bundle bundle = getIntent().getExtras();
            Log.e("bundle", bundle.toString());
            String city = bundle.getString("city");
            String area = bundle.getString("area");
            Log.e("city area", city + " " + area);

            //若在介面1按了搜尋最近廁所則回傳現在座標到介面2
            if(city != null && area != null) {
                if (city.equals("0") && area.equals("0")) {
                    Intent intent = new Intent();
                    intent.setClass(MapsActivity.this, Main2Activity.class);

                    Bundle bundle2 = new Bundle();
                    bundle2.putString("city", String.valueOf(0));
                    bundle2.putString("area", String.valueOf(0));
                    bundle2.putString("lat", String.valueOf(lat));
                    bundle2.putString("lng", String.valueOf(lng));

                    intent.putExtras(bundle2);
                    //Log.e("key3", city + " " + area);

                    startActivity(intent);
                    MapsActivity.this.finish();
                }
            }
            //若點擊清單上的資料則只顯示一筆在地圖上
            if(bundle.getBoolean("onePosition")){
                lat = bundle.getDouble("lat");
                lng = bundle.getDouble("lng");

                sydney = new LatLng(lat, lng);
                mMap.addMarker(new MarkerOptions().position(sydney).title("目標位置"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                Log.e("lat lng",lat+" "+lng);
            }
            //若按googlemap按鈕則顯示前十筆資料
            else if(bundle.getBoolean("onePosition") == false){
                Bundle posBundle = getIntent().getExtras();
                String[] posName = new String[20];
                Double[] posLat = new Double[20];
                Double[] posLng = new Double[20];
                for(int i=0; i<20; i++){
                    posName[i] = posBundle.getString("posName"+i);
                    posLat[i] = posBundle.getDouble("posLat"+i);
                    posLng[i] = posBundle.getDouble("posLng"+i);
                    mMap.addMarker(new MarkerOptions().position(new LatLng(posLat[i], posLng[i])).title(posName[i]));
                    Log.e("name lat lng:"+i, posName[i]+" "+posLat[i]+" "+posLng[i]);
                }
                mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(posLat[0], posLng[0])));
            }
            Log.e("onePosition", String.valueOf(bundle.getBoolean("onePosition")));
        }


        mMap.setMyLocationEnabled(true); // 右上角的定位功能；這行會出現紅色底線，不過仍可正常編譯執行
        mMap.getUiSettings().setZoomControlsEnabled(true);  // 右下角的放大縮小功能
        mMap.getUiSettings().setCompassEnabled(true);       // 左上角的指南針，要兩指旋轉才會出現
        mMap.getUiSettings().setMapToolbarEnabled(true);    // 右下角的導覽及開啟 Google Map功能


        mMap.animateCamera(CameraUpdateFactory.zoomTo(16));     // 放大地圖到 16 倍大
    }
}
