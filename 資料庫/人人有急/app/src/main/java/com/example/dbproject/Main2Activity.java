package com.example.dbproject;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Canvas;
import android.location.Location;
import android.location.LocationManager;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;

import javax.net.ssl.HttpsURLConnection;
import static android.R.layout.simple_list_item_1;

public class Main2Activity extends AppCompatActivity {

    TextView txt;
    Button btnMap, btnLast;
    ListView list;
    MyHandler mh;
    String[] arr;
    String city, area;
    int count;
    double[] posLat;
    double[] posLng;
    Intent intentMap = new Intent();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        this.setTitle("人人有急");

        txt = (TextView) this.findViewById(R.id.txt);
        btnLast = (Button) this.findViewById(R.id.btnLast);
        btnMap = (Button) this.findViewById(R.id.btnMap);
        list = (ListView) this.findViewById(R.id.list);
        mh = new MyHandler();

        //回到上一頁
        btnLast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentLast = new Intent();
                intentLast.setClass(Main2Activity.this, MainActivity.class);
                startActivity(intentLast);
                Main2Activity.this.finish();
            }
        });
        //開啟GoogleMap
        btnMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentMap.setClass(Main2Activity.this, MapsActivity.class);
                intentMap.putExtra("onePosition",false);
                startActivity(intentMap);
            }
        });

        //擷取資料
        Thread thread = new Thread(){
            public void run(){
                //獲取傳來的city跟area
                Bundle bundle = getIntent().getExtras();
                city = bundle.getString("city");
                area = bundle.getString("area");
                String urlString;
                Log.e("city and area from1",city+" "+area);
                if(city.equals("0") && area.equals("0")){
                    String lat = bundle.getString("lat");
                    String lng = bundle.getString("lng");
                    urlString = "http://114.35.120.169:8081/?city=" + city + "&area=" + area + "&lat=" + lat + "&lng=" + lng;
                }
                else{
                    urlString = "http://114.35.120.169:8081/?city=" + city + "&area=" + area;
                }
                HttpURLConnection connection = null;
                try {
                    // 初始化 URL
                    URL url = new URL(urlString);
                    // 取得連線物件
                    connection = (HttpURLConnection) url.openConnection();
                    // 設定 request timeout
                    connection.setReadTimeout(150000);
                    connection.setConnectTimeout(150000);
                    // 模擬 Chrome 的 user agent, 因為手機的網頁內容較不完整
                    connection.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.71 Safari/537.36");
                    // 設定開啟自動轉址
                    connection.setInstanceFollowRedirects(true);

                    // 若要求回傳 200 OK 表示成功取得網頁內容
                    if( connection.getResponseCode() == HttpsURLConnection.HTTP_OK ){
                        // 讀取網頁內容
                        InputStream inputStream     = connection.getInputStream();
                        BufferedReader bufferedReader  = new BufferedReader( new InputStreamReader(inputStream) );

                        String tempStr;
                        StringBuffer stringBuffer = new StringBuffer();

                        while( ( tempStr = bufferedReader.readLine() ) != null ) {
                            stringBuffer.append( tempStr );
                        }

                        bufferedReader.close();
                        inputStream.close();

                        // 取得網頁內容類型
                        String  mime = connection.getContentType();
                        boolean isMediaStream = false;

                        // 判斷是否為串流檔案
                        if( mime.indexOf("audio") == 0 ||  mime.indexOf("video") == 0 ){
                            isMediaStream = true;
                        }

                        // 網頁內容字串
                        String responseString = stringBuffer.toString();
                        Log.e("string", String.valueOf(responseString.charAt(442)));
                        Log.e("string", String.valueOf(responseString.charAt(443)));
                        Log.e("string", String.valueOf(responseString.charAt(444)));
                        //轉換json array成string
                        try{
                            JSONArray mJsonArray = new JSONArray(responseString);
                            JSONObject[] mJsonObject = new JSONObject[mJsonArray.length()];
                            arr = new String[mJsonArray.length()];
                            posLat = new double[mJsonArray.length()];
                            posLng = new double[mJsonArray.length()];
                            //
                            Bundle posBundle = new Bundle();
                            for(int i=0; i<mJsonArray.length(); i++){
                                mJsonObject[i] = mJsonArray.getJSONObject(i);
                                arr[i] = mJsonObject[i].getString("name") + "\n" + mJsonObject[i].getString("address");
                                posLat[i] = mJsonObject[i].getDouble("lat");
                                posLng[i] = mJsonObject[i].getDouble("lng");

                                posBundle.putString("posName"+i,mJsonObject[i].getString("name"));
                                posBundle.putDouble("posLat"+i,posLat[i]);
                                posBundle.putDouble("posLng"+i,posLng[i]);

                                intentMap.putExtras(posBundle);
                            }
                            count = mJsonArray.length();

                        }
                        catch (Exception e){
                            Log.d("e", e.toString());
                            Log.e("ex", "exception happened!");
                        }
                        //傳訊息給handler
                        Message msg = new Message();
                        msg.what = 1;
                        mh.sendMessage(msg);
                        Log.d("net", responseString);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("ex", "exception happened!2");
                }
                finally {
                    // 中斷連線
                    if( connection != null ) {
                        connection.disconnect();
                    }
                }
            }
        };
        thread.start();

        //點擊item可切換到map顯示位置
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                new AlertDialog.Builder(Main2Activity.this)
                        .setTitle("Google Map")
                        .setMessage("在Google Map上顯示此廁所位置?")
                        .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intentList = new Intent();
                                intentList.setClass(Main2Activity.this, MapsActivity.class);

                                Bundle bundleList = new Bundle();
                                //city和area設為1，否則會以為是要獲取現在座標而跳回來
                                city = "1";
                                area = "1";
                                bundleList.putString("city",city);
                                bundleList.putString("area",area);
                                bundleList.putBoolean("onePosition",true);
                                bundleList.putDouble("lat",posLat[position]);
                                bundleList.putDouble("lng",posLng[position]);
                                Log.e("pos(lat lng)",posLat[position] + " " + posLng[position]);

                                intentList.putExtras(bundleList);
                                Log.e("key",city+" "+area);

                                startActivity(intentList);
                            }
                        })
                        .setNegativeButton("取消", null)
                        .show();
            }
        });

    }

    //處理執行緒改變ui
    class MyHandler extends Handler{
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case 1:
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.simple_list_item_1, R.id.text1, arr);
                    list.setAdapter(adapter);
                    txt.setText("共 " + count + " 筆資料");
                    break;
            }
        }
    }



}
