package com.example.dbproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Toast;

import static android.view.View.VISIBLE;
import static com.example.dbproject.R.id.checkBox;

public class MainActivity extends AppCompatActivity {
    String area;
    String city;
    private CheckBox checkBox1;//taipei
    private CheckBox checkBox2;
    private CheckBox checkBox3;
    private CheckBox checkBox4;
    private CheckBox checkBox5;
    private CheckBox checkBox6;
    private CheckBox checkBox7;
    private CheckBox checkBox8;
    private CheckBox checkBox9;
    private CheckBox checkBox10;
    private CheckBox checkBox11;
    private CheckBox checkBox12;

    private CheckBox checkBox13;//shinpei
    private CheckBox checkBox14;
    private CheckBox checkBox15;
    private CheckBox checkBox16;
    private CheckBox checkBox17;
    private CheckBox checkBox18;
    private CheckBox checkBox19;
    private CheckBox checkBox20;
    private CheckBox checkBox21;
    private CheckBox checkBox22;
    private CheckBox checkBox23;
    private CheckBox checkBox24;
    private CheckBox checkBox25;
    private CheckBox checkBox26;
    private CheckBox checkBox27;
    private CheckBox checkBox28;
    private CheckBox checkBox29;
    private CheckBox checkBox30;
    private CheckBox checkBox31;
    private CheckBox checkBox32;
    private CheckBox checkBox33;
    private CheckBox checkBox34;
    private CheckBox checkBox35;
    private CheckBox checkBox36;
    private CheckBox checkBox37;
    private CheckBox checkBox38;
    private CheckBox checkBox39;
    private CheckBox checkBox40;
    private CheckBox checkBox41;

    private CheckBox checkBox42;//kaoshung
    private CheckBox checkBox43;
    private CheckBox checkBox44;
    private CheckBox checkBox45;
    private CheckBox checkBox46;
    private CheckBox checkBox47;
    private CheckBox checkBox48;
    private CheckBox checkBox49;
    private CheckBox checkBox57;
    private CheckBox checkBox58;

    private CheckBox checkBox50;//jilong
    private CheckBox checkBox51;
    private CheckBox checkBox52;
    private CheckBox checkBox53;
    private CheckBox checkBox54;
    private CheckBox checkBox55;
    private CheckBox checkBox56;

    private CheckBox checkBox59;//taoyuan
    private CheckBox checkBox60;
    private CheckBox checkBox61;
    private CheckBox checkBox62;
    private CheckBox checkBox63;
    private CheckBox checkBox64;
    private CheckBox checkBox65;
    private CheckBox checkBox66;
    private CheckBox checkBox67;
    private CheckBox checkBox68;
    private CheckBox checkBox69;
    private CheckBox checkBox70;
    private CheckBox checkBox71;

    private CheckBox checkBox72;
    private CheckBox checkBox73;
    private CheckBox checkBox74;
    private CheckBox checkBox75;
    private CheckBox checkBox76;
    private CheckBox checkBox77;
    private CheckBox checkBox78;
    private CheckBox checkBox79;
    private CheckBox checkBox80;
    private CheckBox checkBox81;
    private CheckBox checkBox82;
    private CheckBox checkBox83;
    private CheckBox checkBox84;
    private CheckBox checkBox85;
    private CheckBox checkBox86;
    private CheckBox checkBox87;
    private CheckBox checkBox88;
    private CheckBox checkBox89;
    private CheckBox checkBox90;
    private CheckBox checkBox91;
    private CheckBox checkBox92;
    private CheckBox checkBox93;
    private CheckBox checkBox94;
    private CheckBox checkBox95;
    private CheckBox checkBox96;
    private CheckBox checkBox97;
    private CheckBox checkBox98;
    private CheckBox checkBox99;
    private CheckBox checkBox100;

    private CheckBox checkBox101;
    private CheckBox checkBox102;
    private CheckBox checkBox103;
    private CheckBox checkBox104;
    private CheckBox checkBox105;
    private CheckBox checkBox106;
    private CheckBox checkBox107;
    private CheckBox checkBox108;
    private CheckBox checkBox109;
    private CheckBox checkBox110;
    private CheckBox checkBox111;
    private CheckBox checkBox112;
    private CheckBox checkBox113;
    private CheckBox checkBox114;
    private CheckBox checkBox115;
    private CheckBox checkBox116;
    private CheckBox checkBox117;
    private CheckBox checkBox118;
    private CheckBox checkBox119;
    private CheckBox checkBox120;

    private CheckBox checkBox121;//tainan
    private CheckBox checkBox122;
    private CheckBox checkBox123;
    private CheckBox checkBox124;
    private CheckBox checkBox125;
    private CheckBox checkBox126;
    private CheckBox checkBox127;
    private CheckBox checkBox128;
    private CheckBox checkBox129;
    private CheckBox checkBox130;
    private CheckBox checkBox131;
    private CheckBox checkBox132;
    private CheckBox checkBox133;
    private CheckBox checkBox134;
    private CheckBox checkBox135;
    private CheckBox checkBox136;
    private CheckBox checkBox137;
    private CheckBox checkBox138;
    private CheckBox checkBox139;
    private CheckBox checkBox140;
    private CheckBox checkBox141;
    private CheckBox checkBox142;
    private CheckBox checkBox143;
    private CheckBox checkBox144;
    private CheckBox checkBox145;
    private CheckBox checkBox146;
    private CheckBox checkBox147;
    private CheckBox checkBox148;
    private CheckBox checkBox149;
    private CheckBox checkBox150;
    private CheckBox checkBox151;
    private CheckBox checkBox152;
    private CheckBox checkBox153;
    private CheckBox checkBox154;
    private CheckBox checkBox155;
    private CheckBox checkBox156;
    private CheckBox checkBox157;

    private CheckBox checkBox158;
    private CheckBox checkBox159;
    private CheckBox checkBox160;
    private CheckBox checkBox161;
    private CheckBox checkBox162;
    private CheckBox checkBox163;
    private CheckBox checkBox164;
    private CheckBox checkBox165;
    private CheckBox checkBox166;
    private CheckBox checkBox167;
    private CheckBox checkBox168;
    private CheckBox checkBox169;
    private CheckBox checkBox170;

    private CheckBox checkBox171;
    private CheckBox checkBox172;
    private CheckBox checkBox173;
    private CheckBox checkBox174;
    private CheckBox checkBox175;
    private CheckBox checkBox176;
    private CheckBox checkBox177;
    private CheckBox checkBox178;
    private CheckBox checkBox179;
    private CheckBox checkBox180;
    private CheckBox checkBox181;
    private CheckBox checkBox182;
    private CheckBox checkBox183;
    private CheckBox checkBox184;
    private CheckBox checkBox185;
    private CheckBox checkBox186;
    private CheckBox checkBox187;
    private CheckBox checkBox188;

    private CheckBox checkBox189;
    private CheckBox checkBox190;
    private CheckBox checkBox191;
    private CheckBox checkBox192;
    private CheckBox checkBox193;
    private CheckBox checkBox194;
    private CheckBox checkBox195;
    private CheckBox checkBox196;
    private CheckBox checkBox197;
    private CheckBox checkBox198;
    private CheckBox checkBox199;
    private CheckBox checkBox200;
    private CheckBox checkBox201;
    private CheckBox checkBox202;
    private CheckBox checkBox203;
    private CheckBox checkBox204;

    private CheckBox checkBox205;
    private CheckBox checkBox206;
    private CheckBox checkBox207;
    private CheckBox checkBox208;
    private CheckBox checkBox209;
    private CheckBox checkBox210;
    private CheckBox checkBox211;
    private CheckBox checkBox212;
    private CheckBox checkBox213;
    private CheckBox checkBox214;
    private CheckBox checkBox215;
    private CheckBox checkBox216;
    private CheckBox checkBox217;
    private CheckBox checkBox218;
    private CheckBox checkBox219;
    private CheckBox checkBox220;
    private CheckBox checkBox221;
    private CheckBox checkBox222;
    private CheckBox checkBox223;
    private CheckBox checkBox224;
    private CheckBox checkBox225;
    private CheckBox checkBox226;
    private CheckBox checkBox227;
    private CheckBox checkBox228;
    private CheckBox checkBox229;
    private CheckBox checkBox230;
    private CheckBox checkBox231;
    private CheckBox checkBox232;
    private CheckBox checkBox233;
    private CheckBox checkBox234;
    private CheckBox checkBox235;
    private CheckBox checkBox236;
    private CheckBox checkBox237;
    private CheckBox checkBox238;
    private CheckBox checkBox239;
    private CheckBox checkBox240;
    private CheckBox checkBox241;
    private CheckBox checkBox242;
    private CheckBox checkBox243;
    private CheckBox checkBox244;
    private CheckBox checkBox245;
    private CheckBox checkBox246;
    private CheckBox checkBox247;
    private CheckBox checkBox248;
    private CheckBox checkBox249;

    private CheckBox checkBox250;
    private CheckBox checkBox251;
    private CheckBox checkBox252;
    private CheckBox checkBox253;
    private CheckBox checkBox254;
    private CheckBox checkBox255;
    private CheckBox checkBox256;
    private CheckBox checkBox257;
    private CheckBox checkBox258;
    private CheckBox checkBox259;
    private CheckBox checkBox260;
    private CheckBox checkBox261;
    private CheckBox checkBox262;
    private CheckBox checkBox263;
    private CheckBox checkBox264;
    private CheckBox checkBox265;
    private CheckBox checkBox266;
    private CheckBox checkBox267;
    private CheckBox checkBox268;
    private CheckBox checkBox269;
    private CheckBox checkBox270;
    private CheckBox checkBox271;
    private CheckBox checkBox272;
    private CheckBox checkBox273;
    private CheckBox checkBox274;

    private CheckBox checkBox275;
    private CheckBox checkBox276;
    private CheckBox checkBox277;

    private CheckBox checkBox278;
    private CheckBox checkBox279;
    private CheckBox checkBox280;
    private CheckBox checkBox281;
    private CheckBox checkBox282;
    private CheckBox checkBox283;
    private CheckBox checkBox284;
    private CheckBox checkBox285;
    private CheckBox checkBox286;
    private CheckBox checkBox287;
    private CheckBox checkBox288;
    private CheckBox checkBox289;
    private CheckBox checkBox290;


    private Spinner spin;
    private Button btn;
    private Button btnDistance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.setTitle("人人有急");
        checkBox1 = (CheckBox) findViewById(checkBox);//taipei
        checkBox2 = (CheckBox) findViewById(R.id.checkBox3);
        checkBox3 = (CheckBox) findViewById(R.id.checkBox4);
        checkBox4 = (CheckBox) findViewById(R.id.checkBox5);
        checkBox5 = (CheckBox) findViewById(R.id.checkBox6);
        checkBox6 = (CheckBox) findViewById(R.id.checkBox7);
        checkBox7 = (CheckBox) findViewById(R.id.checkBox8);
        checkBox8 = (CheckBox) findViewById(R.id.checkBox9);
        checkBox9 = (CheckBox) findViewById(R.id.checkBox11);
        checkBox10 = (CheckBox) findViewById(R.id.checkBox12);
        checkBox11 = (CheckBox) findViewById(R.id.checkBox13);
        checkBox12 = (CheckBox) findViewById(R.id.checkBox14);
        //shinpei
        checkBox13 = (CheckBox) findViewById(R.id.checkBox15);
        checkBox14 = (CheckBox) findViewById(R.id.checkBox16);
        checkBox15 = (CheckBox) findViewById(R.id.checkBox17);
        checkBox16 = (CheckBox) findViewById(R.id.checkBox18);
        checkBox17 = (CheckBox) findViewById(R.id.checkBox19);
        checkBox18 = (CheckBox) findViewById(R.id.checkBox20);
        checkBox19 = (CheckBox) findViewById(R.id.checkBox21);
        checkBox20 = (CheckBox) findViewById(R.id.checkBox22);
        checkBox21 = (CheckBox) findViewById(R.id.checkBox23);
        checkBox22 = (CheckBox) findViewById(R.id.checkBox24);
        checkBox23 = (CheckBox) findViewById(R.id.checkBox25);
        checkBox24 = (CheckBox) findViewById(R.id.checkBox26);
        checkBox25 = (CheckBox) findViewById(R.id.checkBox27);
        checkBox26 = (CheckBox) findViewById(R.id.checkBox28);
        checkBox27 = (CheckBox) findViewById(R.id.checkBox29);
        checkBox28 = (CheckBox) findViewById(R.id.checkBox30);
        checkBox29 = (CheckBox) findViewById(R.id.checkBox31);
        checkBox30 = (CheckBox) findViewById(R.id.checkBox32);
        checkBox31 = (CheckBox) findViewById(R.id.checkBox33);
        checkBox32 = (CheckBox) findViewById(R.id.checkBox34);
        checkBox33 = (CheckBox) findViewById(R.id.checkBox35);
        checkBox34 = (CheckBox) findViewById(R.id.checkBox36);
        checkBox35 = (CheckBox) findViewById(R.id.checkBox37);
        checkBox36 = (CheckBox) findViewById(R.id.checkBox38);
        checkBox37 = (CheckBox) findViewById(R.id.checkBox39);
        checkBox38 = (CheckBox) findViewById(R.id.checkBox40);
        checkBox39 = (CheckBox) findViewById(R.id.checkBox41);
        checkBox40 = (CheckBox) findViewById(R.id.checkBox2);
        checkBox41 = (CheckBox) findViewById(R.id.checkBox10);
        //kaoshung
        checkBox42 = (CheckBox) findViewById(R.id.checkBox42);
        checkBox43 = (CheckBox) findViewById(R.id.checkBox43);
        checkBox44 = (CheckBox) findViewById(R.id.checkBox44);
        checkBox45 = (CheckBox) findViewById(R.id.checkBox45);
        checkBox46 = (CheckBox) findViewById(R.id.checkBox46);
        checkBox47 = (CheckBox) findViewById(R.id.checkBox47);
        checkBox48 = (CheckBox) findViewById(R.id.checkBox48);
        checkBox49 = (CheckBox) findViewById(R.id.checkBox49);
        checkBox57 = (CheckBox) findViewById(R.id.checkBox57);
        checkBox58 = (CheckBox) findViewById(R.id.checkBox58);
        //jilong
        checkBox50 = (CheckBox) findViewById(R.id.checkBox50);
        checkBox51 = (CheckBox) findViewById(R.id.checkBox51);
        checkBox52 = (CheckBox) findViewById(R.id.checkBox52);
        checkBox53 = (CheckBox) findViewById(R.id.checkBox53);
        checkBox54 = (CheckBox) findViewById(R.id.checkBox54);
        checkBox55 = (CheckBox) findViewById(R.id.checkBox55);
        checkBox56 = (CheckBox) findViewById(R.id.checkBox56);
        //taoyuan
        checkBox59 = (CheckBox) findViewById(R.id.checkBox59);
        checkBox60 = (CheckBox) findViewById(R.id.checkBox60);
        checkBox61 = (CheckBox) findViewById(R.id.checkBox61);
        checkBox62 = (CheckBox) findViewById(R.id.checkBox62);
        checkBox63 = (CheckBox) findViewById(R.id.checkBox63);
        checkBox64 = (CheckBox) findViewById(R.id.checkBox64);
        checkBox65 = (CheckBox) findViewById(R.id.checkBox65);
        checkBox66 = (CheckBox) findViewById(R.id.checkBox66);
        checkBox67 = (CheckBox) findViewById(R.id.checkBox67);
        checkBox68 = (CheckBox) findViewById(R.id.checkBox68);
        checkBox69 = (CheckBox) findViewById(R.id.checkBox69);
        checkBox70 = (CheckBox) findViewById(R.id.checkBox70);
        checkBox71 = (CheckBox) findViewById(R.id.checkBox71);
        //taichung
        checkBox72 = (CheckBox) findViewById(R.id.checkBox72);
        checkBox73 = (CheckBox) findViewById(R.id.checkBox73);
        checkBox74 = (CheckBox) findViewById(R.id.checkBox74);
        checkBox75 = (CheckBox) findViewById(R.id.checkBox75);
        checkBox76 = (CheckBox) findViewById(R.id.checkBox76);
        checkBox77 = (CheckBox) findViewById(R.id.checkBox77);
        checkBox78 = (CheckBox) findViewById(R.id.checkBox78);
        checkBox79 = (CheckBox) findViewById(R.id.checkBox79);
        checkBox80 = (CheckBox) findViewById(R.id.checkBox80);
        checkBox81 = (CheckBox) findViewById(R.id.checkBox81);
        checkBox82 = (CheckBox) findViewById(R.id.checkBox82);
        checkBox83 = (CheckBox) findViewById(R.id.checkBox83);
        checkBox84 = (CheckBox) findViewById(R.id.checkBox84);
        checkBox85 = (CheckBox) findViewById(R.id.checkBox85);
        checkBox86 = (CheckBox) findViewById(R.id.checkBox86);
        checkBox87 = (CheckBox) findViewById(R.id.checkBox87);
        checkBox88 = (CheckBox) findViewById(R.id.checkBox88);
        checkBox89 = (CheckBox) findViewById(R.id.checkBox89);
        checkBox90 = (CheckBox) findViewById(R.id.checkBox90);
        checkBox91 = (CheckBox) findViewById(R.id.checkBox91);
        checkBox92 = (CheckBox) findViewById(R.id.checkBox92);
        checkBox93 = (CheckBox) findViewById(R.id.checkBox93);
        checkBox94 = (CheckBox) findViewById(R.id.checkBox94);
        checkBox95 = (CheckBox) findViewById(R.id.checkBox95);
        checkBox96 = (CheckBox) findViewById(R.id.checkBox96);
        checkBox97 = (CheckBox) findViewById(R.id.checkBox97);
        checkBox98 = (CheckBox) findViewById(R.id.checkBox98);
        checkBox99 = (CheckBox) findViewById(R.id.checkBox99);
        checkBox100 = (CheckBox) findViewById(R.id.checkBox100);
        //yunlin
        checkBox101 = (CheckBox) findViewById(R.id.checkBox101);
        checkBox102 = (CheckBox) findViewById(R.id.checkBox102);
        checkBox103 = (CheckBox) findViewById(R.id.checkBox103);
        checkBox104 = (CheckBox) findViewById(R.id.checkBox104);
        checkBox105 = (CheckBox) findViewById(R.id.checkBox105);
        checkBox106 = (CheckBox) findViewById(R.id.checkBox106);
        checkBox107 = (CheckBox) findViewById(R.id.checkBox107);
        checkBox108 = (CheckBox) findViewById(R.id.checkBox108);
        checkBox109 = (CheckBox) findViewById(R.id.checkBox109);
        checkBox110 = (CheckBox) findViewById(R.id.checkBox110);
        checkBox111 = (CheckBox) findViewById(R.id.checkBox111);
        checkBox112 = (CheckBox) findViewById(R.id.checkBox112);
        checkBox113 = (CheckBox) findViewById(R.id.checkBox113);
        checkBox114 = (CheckBox) findViewById(R.id.checkBox114);
        checkBox115 = (CheckBox) findViewById(R.id.checkBox115);
        checkBox116 = (CheckBox) findViewById(R.id.checkBox116);
        checkBox117 = (CheckBox) findViewById(R.id.checkBox117);
        checkBox118 = (CheckBox) findViewById(R.id.checkBox118);
        checkBox119 = (CheckBox) findViewById(R.id.checkBox119);
        checkBox120 = (CheckBox) findViewById(R.id.checkBox120);
        //tainan
        checkBox121 = (CheckBox) findViewById(R.id.checkBox121);
        checkBox122 = (CheckBox) findViewById(R.id.checkBox122);
        checkBox123 = (CheckBox) findViewById(R.id.checkBox123);
        checkBox124 = (CheckBox) findViewById(R.id.checkBox124);
        checkBox125 = (CheckBox) findViewById(R.id.checkBox125);
        checkBox126 = (CheckBox) findViewById(R.id.checkBox126);
        checkBox127 = (CheckBox) findViewById(R.id.checkBox127);
        checkBox128 = (CheckBox) findViewById(R.id.checkBox128);
        checkBox129 = (CheckBox) findViewById(R.id.checkBox129);
        checkBox130 = (CheckBox) findViewById(R.id.checkBox130);
        checkBox131 = (CheckBox) findViewById(R.id.checkBox131);
        checkBox132 = (CheckBox) findViewById(R.id.checkBox132);
        checkBox133 = (CheckBox) findViewById(R.id.checkBox133);
        checkBox134 = (CheckBox) findViewById(R.id.checkBox134);
        checkBox135 = (CheckBox) findViewById(R.id.checkBox135);
        checkBox136 = (CheckBox) findViewById(R.id.checkBox136);
        checkBox137 = (CheckBox) findViewById(R.id.checkBox137);
        checkBox138 = (CheckBox) findViewById(R.id.checkBox138);
        checkBox139 = (CheckBox) findViewById(R.id.checkBox139);
        checkBox140 = (CheckBox) findViewById(R.id.checkBox140);
        checkBox141 = (CheckBox) findViewById(R.id.checkBox141);
        checkBox142 = (CheckBox) findViewById(R.id.checkBox142);
        checkBox143 = (CheckBox) findViewById(R.id.checkBox143);
        checkBox144 = (CheckBox) findViewById(R.id.checkBox144);
        checkBox145 = (CheckBox) findViewById(R.id.checkBox145);
        checkBox146 = (CheckBox) findViewById(R.id.checkBox146);
        checkBox147 = (CheckBox) findViewById(R.id.checkBox147);
        checkBox148 = (CheckBox) findViewById(R.id.checkBox148);
        checkBox149 = (CheckBox) findViewById(R.id.checkBox149);
        checkBox150 = (CheckBox) findViewById(R.id.checkBox150);
        checkBox151 = (CheckBox) findViewById(R.id.checkBox151);
        checkBox152 = (CheckBox) findViewById(R.id.checkBox152);
        checkBox153 = (CheckBox) findViewById(R.id.checkBox153);
        checkBox154 = (CheckBox) findViewById(R.id.checkBox154);
        checkBox155 = (CheckBox) findViewById(R.id.checkBox155);
        checkBox156 = (CheckBox) findViewById(R.id.checkBox156);
        checkBox157 = (CheckBox) findViewById(R.id.checkBox157);
        //hualien
        checkBox158 = (CheckBox) findViewById(R.id.checkBox158);
        checkBox159 = (CheckBox) findViewById(R.id.checkBox159);
        checkBox160 = (CheckBox) findViewById(R.id.checkBox160);
        checkBox161 = (CheckBox) findViewById(R.id.checkBox161);
        checkBox162 = (CheckBox) findViewById(R.id.checkBox162);
        checkBox163 = (CheckBox) findViewById(R.id.checkBox163);
        checkBox164 = (CheckBox) findViewById(R.id.checkBox164);
        checkBox165 = (CheckBox) findViewById(R.id.checkBox165);
        checkBox166 = (CheckBox) findViewById(R.id.checkBox166);
        checkBox167 = (CheckBox) findViewById(R.id.checkBox167);
        checkBox168 = (CheckBox) findViewById(R.id.checkBox168);
        checkBox169 = (CheckBox) findViewById(R.id.checkBox169);
        checkBox170 = (CheckBox) findViewById(R.id.checkBox170);
        //miaoli
        checkBox171 = (CheckBox) findViewById(R.id.checkBox171);
        checkBox172 = (CheckBox) findViewById(R.id.checkBox172);
        checkBox173 = (CheckBox) findViewById(R.id.checkBox173);
        checkBox174 = (CheckBox) findViewById(R.id.checkBox174);
        checkBox175 = (CheckBox) findViewById(R.id.checkBox175);
        checkBox176 = (CheckBox) findViewById(R.id.checkBox176);
        checkBox177 = (CheckBox) findViewById(R.id.checkBox177);
        checkBox178 = (CheckBox) findViewById(R.id.checkBox178);
        checkBox179 = (CheckBox) findViewById(R.id.checkBox179);
        checkBox180 = (CheckBox) findViewById(R.id.checkBox180);
        checkBox181 = (CheckBox) findViewById(R.id.checkBox181);
        checkBox182 = (CheckBox) findViewById(R.id.checkBox182);
        checkBox183 = (CheckBox) findViewById(R.id.checkBox183);
        checkBox184 = (CheckBox) findViewById(R.id.checkBox184);
        checkBox185 = (CheckBox) findViewById(R.id.checkBox185);
        checkBox186 = (CheckBox) findViewById(R.id.checkBox186);
        checkBox187 = (CheckBox) findViewById(R.id.checkBox187);
        checkBox188 = (CheckBox) findViewById(R.id.checkBox188);
        //taidong
        checkBox189 = (CheckBox) findViewById(R.id.checkBox189);
        checkBox190 = (CheckBox) findViewById(R.id.checkBox190);
        checkBox191 = (CheckBox) findViewById(R.id.checkBox191);
        checkBox192 = (CheckBox) findViewById(R.id.checkBox192);
        checkBox193 = (CheckBox) findViewById(R.id.checkBox193);
        checkBox194 = (CheckBox) findViewById(R.id.checkBox194);
        checkBox195 = (CheckBox) findViewById(R.id.checkBox195);
        checkBox196 = (CheckBox) findViewById(R.id.checkBox196);
        checkBox197 = (CheckBox) findViewById(R.id.checkBox197);
        checkBox198 = (CheckBox) findViewById(R.id.checkBox198);
        checkBox199 = (CheckBox) findViewById(R.id.checkBox199);
        checkBox200 = (CheckBox) findViewById(R.id.checkBox200);
        checkBox201 = (CheckBox) findViewById(R.id.checkBox201);
        checkBox202 = (CheckBox) findViewById(R.id.checkBox202);
        checkBox203 = (CheckBox) findViewById(R.id.checkBox203);
        checkBox204 = (CheckBox) findViewById(R.id.checkBox204);
        //pindong
        checkBox205 = (CheckBox) findViewById(R.id.checkBox205);
        checkBox206 = (CheckBox) findViewById(R.id.checkBox206);
        checkBox207 = (CheckBox) findViewById(R.id.checkBox207);
        checkBox208 = (CheckBox) findViewById(R.id.checkBox208);
        checkBox209 = (CheckBox) findViewById(R.id.checkBox209);
        checkBox210 = (CheckBox) findViewById(R.id.checkBox210);
        checkBox211 = (CheckBox) findViewById(R.id.checkBox211);
        checkBox212 = (CheckBox) findViewById(R.id.checkBox212);
        checkBox213 = (CheckBox) findViewById(R.id.checkBox213);
        checkBox214 = (CheckBox) findViewById(R.id.checkBox214);
        checkBox215 = (CheckBox) findViewById(R.id.checkBox215);
        checkBox216 = (CheckBox) findViewById(R.id.checkBox216);
        checkBox217 = (CheckBox) findViewById(R.id.checkBox217);
        checkBox218 = (CheckBox) findViewById(R.id.checkBox218);
        checkBox219 = (CheckBox) findViewById(R.id.checkBox219);
        checkBox220 = (CheckBox) findViewById(R.id.checkBox220);
        checkBox221 = (CheckBox) findViewById(R.id.checkBox221);
        checkBox222 = (CheckBox) findViewById(R.id.checkBox222);
        checkBox223 = (CheckBox) findViewById(R.id.checkBox223);
        checkBox224 = (CheckBox) findViewById(R.id.checkBox224);
        checkBox225 = (CheckBox) findViewById(R.id.checkBox225);
        checkBox226 = (CheckBox) findViewById(R.id.checkBox226);
        checkBox227 = (CheckBox) findViewById(R.id.checkBox227);
        checkBox228 = (CheckBox) findViewById(R.id.checkBox228);
        checkBox229 = (CheckBox) findViewById(R.id.checkBox229);
        checkBox230 = (CheckBox) findViewById(R.id.checkBox230);
        checkBox231 = (CheckBox) findViewById(R.id.checkBox231);
        checkBox232 = (CheckBox) findViewById(R.id.checkBox232);
        checkBox233 = (CheckBox) findViewById(R.id.checkBox233);
        checkBox234 = (CheckBox) findViewById(R.id.checkBox234);
        checkBox235 = (CheckBox) findViewById(R.id.checkBox235);
        checkBox236 = (CheckBox) findViewById(R.id.checkBox236);
        //shinchucount
        checkBox237 = (CheckBox) findViewById(R.id.checkBox237);
        checkBox238 = (CheckBox) findViewById(R.id.checkBox238);
        checkBox239 = (CheckBox) findViewById(R.id.checkBox239);
        checkBox240 = (CheckBox) findViewById(R.id.checkBox240);
        checkBox241 = (CheckBox) findViewById(R.id.checkBox241);
        checkBox242 = (CheckBox) findViewById(R.id.checkBox242);
        checkBox243 = (CheckBox) findViewById(R.id.checkBox243);
        checkBox244 = (CheckBox) findViewById(R.id.checkBox244);
        checkBox245 = (CheckBox) findViewById(R.id.checkBox245);
        checkBox246 = (CheckBox) findViewById(R.id.checkBox246);
        checkBox247 = (CheckBox) findViewById(R.id.checkBox247);
        checkBox248 = (CheckBox) findViewById(R.id.checkBox248);
        checkBox249 = (CheckBox) findViewById(R.id.checkBox249);

        checkBox250 = (CheckBox) findViewById(R.id.checkBox250);
        checkBox251 = (CheckBox) findViewById(R.id.checkBox251);
        checkBox252 = (CheckBox) findViewById(R.id.checkBox252);
        checkBox253 = (CheckBox) findViewById(R.id.checkBox253);
        checkBox254 = (CheckBox) findViewById(R.id.checkBox254);
        checkBox255 = (CheckBox) findViewById(R.id.checkBox255);
        checkBox256 = (CheckBox) findViewById(R.id.checkBox256);
        checkBox257 = (CheckBox) findViewById(R.id.checkBox257);
        checkBox258 = (CheckBox) findViewById(R.id.checkBox258);
        checkBox259 = (CheckBox) findViewById(R.id.checkBox259);
        checkBox260 = (CheckBox) findViewById(R.id.checkBox260);
        checkBox261 = (CheckBox) findViewById(R.id.checkBox261);
        checkBox262 = (CheckBox) findViewById(R.id.checkBox262);
        checkBox263 = (CheckBox) findViewById(R.id.checkBox263);
        checkBox264 = (CheckBox) findViewById(R.id.checkBox264);
        checkBox265 = (CheckBox) findViewById(R.id.checkBox265);
        checkBox266 = (CheckBox) findViewById(R.id.checkBox266);
        checkBox267 = (CheckBox) findViewById(R.id.checkBox267);
        checkBox268 = (CheckBox) findViewById(R.id.checkBox268);
        checkBox269 = (CheckBox) findViewById(R.id.checkBox269);
        checkBox270 = (CheckBox) findViewById(R.id.checkBox270);
        checkBox271 = (CheckBox) findViewById(R.id.checkBox271);
        checkBox272 = (CheckBox) findViewById(R.id.checkBox272);
        checkBox273 = (CheckBox) findViewById(R.id.checkBox273);
        checkBox274 = (CheckBox) findViewById(R.id.checkBox274);

        checkBox275 = (CheckBox) findViewById(R.id.checkBox275);
        checkBox276 = (CheckBox) findViewById(R.id.checkBox276);
        checkBox277 = (CheckBox) findViewById(R.id.checkBox277);

        checkBox278 = (CheckBox) findViewById(R.id.checkBox278);
        checkBox279 = (CheckBox) findViewById(R.id.checkBox279);
        checkBox280 = (CheckBox) findViewById(R.id.checkBox280);
        checkBox281 = (CheckBox) findViewById(R.id.checkBox281);
        checkBox282 = (CheckBox) findViewById(R.id.checkBox282);
        checkBox283 = (CheckBox) findViewById(R.id.checkBox283);
        checkBox284 = (CheckBox) findViewById(R.id.checkBox284);
        checkBox285 = (CheckBox) findViewById(R.id.checkBox285);
        checkBox286 = (CheckBox) findViewById(R.id.checkBox286);
        checkBox287 = (CheckBox) findViewById(R.id.checkBox287);
        checkBox288 = (CheckBox) findViewById(R.id.checkBox288);
        checkBox289 = (CheckBox) findViewById(R.id.checkBox289);
        checkBox290 = (CheckBox) findViewById(R.id.checkBox290);


        btn = (Button) findViewById(R.id.button);
        btnDistance = (Button) findViewById(R.id.btnDistance);
        spin = (Spinner) findViewById(R.id.spinner);
        //Spinner spinner = (Spinner)findViewById(spinner);
        ArrayAdapter<CharSequence> locationList = ArrayAdapter.createFromResource(MainActivity.this,
                R.array.location, android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(locationList);
        spin.setOnItemSelectedListener(selectlistener);
        checkBox1.setOnCheckedChangeListener(Taicheck);
        checkBox2.setOnCheckedChangeListener(Taicheck);
        checkBox3.setOnCheckedChangeListener(Taicheck);
        checkBox4.setOnCheckedChangeListener(Taicheck);
        checkBox5.setOnCheckedChangeListener(Taicheck);
        checkBox6.setOnCheckedChangeListener(Taicheck);
        checkBox7.setOnCheckedChangeListener(Taicheck);
        checkBox8.setOnCheckedChangeListener(Taicheck);
        checkBox9.setOnCheckedChangeListener(Taicheck);
        checkBox10.setOnCheckedChangeListener(Taicheck);
        checkBox11.setOnCheckedChangeListener(Taicheck);
        checkBox12.setOnCheckedChangeListener(Taicheck);

        checkBox13.setOnCheckedChangeListener(Shincheck);
        checkBox14.setOnCheckedChangeListener(Shincheck);
        checkBox15.setOnCheckedChangeListener(Shincheck);
        checkBox16.setOnCheckedChangeListener(Shincheck);
        checkBox17.setOnCheckedChangeListener(Shincheck);
        checkBox18.setOnCheckedChangeListener(Shincheck);
        checkBox19.setOnCheckedChangeListener(Shincheck);
        checkBox20.setOnCheckedChangeListener(Shincheck);
        checkBox21.setOnCheckedChangeListener(Shincheck);
        checkBox22.setOnCheckedChangeListener(Shincheck);
        checkBox23.setOnCheckedChangeListener(Shincheck);
        checkBox24.setOnCheckedChangeListener(Shincheck);
        checkBox25.setOnCheckedChangeListener(Shincheck);
        checkBox26.setOnCheckedChangeListener(Shincheck);
        checkBox27.setOnCheckedChangeListener(Shincheck);
        checkBox28.setOnCheckedChangeListener(Shincheck);
        checkBox29.setOnCheckedChangeListener(Shincheck);
        checkBox30.setOnCheckedChangeListener(Shincheck);
        checkBox31.setOnCheckedChangeListener(Shincheck);
        checkBox32.setOnCheckedChangeListener(Shincheck);
        checkBox33.setOnCheckedChangeListener(Shincheck);
        checkBox34.setOnCheckedChangeListener(Shincheck);
        checkBox35.setOnCheckedChangeListener(Shincheck);
        checkBox36.setOnCheckedChangeListener(Shincheck);
        checkBox37.setOnCheckedChangeListener(Shincheck);
        checkBox38.setOnCheckedChangeListener(Shincheck);
        checkBox39.setOnCheckedChangeListener(Shincheck);
        checkBox40.setOnCheckedChangeListener(Shincheck);
        checkBox41.setOnCheckedChangeListener(Shincheck);

        checkBox42.setOnCheckedChangeListener(Kaocheck);
        checkBox43.setOnCheckedChangeListener(Kaocheck);
        checkBox44.setOnCheckedChangeListener(Kaocheck);
        checkBox45.setOnCheckedChangeListener(Kaocheck);
        checkBox46.setOnCheckedChangeListener(Kaocheck);
        checkBox47.setOnCheckedChangeListener(Kaocheck);
        checkBox48.setOnCheckedChangeListener(Kaocheck);
        checkBox49.setOnCheckedChangeListener(Kaocheck);
        checkBox58.setOnCheckedChangeListener(Kaocheck);
        checkBox57.setOnCheckedChangeListener(Kaocheck);

        checkBox50.setOnCheckedChangeListener(Jicheck);
        checkBox51.setOnCheckedChangeListener(Jicheck);
        checkBox52.setOnCheckedChangeListener(Jicheck);
        checkBox53.setOnCheckedChangeListener(Jicheck);
        checkBox54.setOnCheckedChangeListener(Jicheck);
        checkBox55.setOnCheckedChangeListener(Jicheck);
        checkBox56.setOnCheckedChangeListener(Jicheck);

        checkBox59.setOnCheckedChangeListener(Taocheck);
        checkBox60.setOnCheckedChangeListener(Taocheck);
        checkBox61.setOnCheckedChangeListener(Taocheck);
        checkBox62.setOnCheckedChangeListener(Taocheck);
        checkBox63.setOnCheckedChangeListener(Taocheck);
        checkBox64.setOnCheckedChangeListener(Taocheck);
        checkBox65.setOnCheckedChangeListener(Taocheck);
        checkBox66.setOnCheckedChangeListener(Taocheck);
        checkBox67.setOnCheckedChangeListener(Taocheck);
        checkBox68.setOnCheckedChangeListener(Taocheck);
        checkBox69.setOnCheckedChangeListener(Taocheck);
        checkBox70.setOnCheckedChangeListener(Taocheck);
        checkBox71.setOnCheckedChangeListener(Taocheck);

        checkBox72.setOnCheckedChangeListener(Taichungcheck);
        checkBox73.setOnCheckedChangeListener(Taichungcheck);
        checkBox74.setOnCheckedChangeListener(Taichungcheck);
        checkBox75.setOnCheckedChangeListener(Taichungcheck);
        checkBox76.setOnCheckedChangeListener(Taichungcheck);
        checkBox77.setOnCheckedChangeListener(Taichungcheck);
        checkBox78.setOnCheckedChangeListener(Taichungcheck);
        checkBox79.setOnCheckedChangeListener(Taichungcheck);
        checkBox80.setOnCheckedChangeListener(Taichungcheck);
        checkBox81.setOnCheckedChangeListener(Taichungcheck);
        checkBox82.setOnCheckedChangeListener(Taichungcheck);
        checkBox83.setOnCheckedChangeListener(Taichungcheck);
        checkBox84.setOnCheckedChangeListener(Taichungcheck);
        checkBox85.setOnCheckedChangeListener(Taichungcheck);
        checkBox86.setOnCheckedChangeListener(Taichungcheck);
        checkBox87.setOnCheckedChangeListener(Taichungcheck);
        checkBox88.setOnCheckedChangeListener(Taichungcheck);
        checkBox89.setOnCheckedChangeListener(Taichungcheck);
        checkBox80.setOnCheckedChangeListener(Taichungcheck);
        checkBox91.setOnCheckedChangeListener(Taichungcheck);
        checkBox92.setOnCheckedChangeListener(Taichungcheck);
        checkBox93.setOnCheckedChangeListener(Taichungcheck);
        checkBox94.setOnCheckedChangeListener(Taichungcheck);
        checkBox95.setOnCheckedChangeListener(Taichungcheck);
        checkBox96.setOnCheckedChangeListener(Taichungcheck);
        checkBox97.setOnCheckedChangeListener(Taichungcheck);
        checkBox98.setOnCheckedChangeListener(Taichungcheck);
        checkBox99.setOnCheckedChangeListener(Taichungcheck);
        checkBox100.setOnCheckedChangeListener(Taichungcheck);

        checkBox101.setOnCheckedChangeListener(Yuncheck);
        checkBox102.setOnCheckedChangeListener(Yuncheck);
        checkBox103.setOnCheckedChangeListener(Yuncheck);
        checkBox104.setOnCheckedChangeListener(Yuncheck);
        checkBox105.setOnCheckedChangeListener(Yuncheck);
        checkBox106.setOnCheckedChangeListener(Yuncheck);
        checkBox107.setOnCheckedChangeListener(Yuncheck);
        checkBox108.setOnCheckedChangeListener(Yuncheck);
        checkBox109.setOnCheckedChangeListener(Yuncheck);
        checkBox110.setOnCheckedChangeListener(Yuncheck);
        checkBox111.setOnCheckedChangeListener(Yuncheck);
        checkBox112.setOnCheckedChangeListener(Yuncheck);
        checkBox113.setOnCheckedChangeListener(Yuncheck);
        checkBox114.setOnCheckedChangeListener(Yuncheck);
        checkBox115.setOnCheckedChangeListener(Yuncheck);
        checkBox116.setOnCheckedChangeListener(Yuncheck);
        checkBox117.setOnCheckedChangeListener(Yuncheck);
        checkBox118.setOnCheckedChangeListener(Yuncheck);
        checkBox119.setOnCheckedChangeListener(Yuncheck);
        checkBox120.setOnCheckedChangeListener(Yuncheck);

        checkBox121.setOnCheckedChangeListener(Tainancheck);
        checkBox122.setOnCheckedChangeListener(Tainancheck);
        checkBox123.setOnCheckedChangeListener(Tainancheck);
        checkBox124.setOnCheckedChangeListener(Tainancheck);
        checkBox125.setOnCheckedChangeListener(Tainancheck);
        checkBox126.setOnCheckedChangeListener(Tainancheck);
        checkBox127.setOnCheckedChangeListener(Tainancheck);
        checkBox128.setOnCheckedChangeListener(Tainancheck);
        checkBox129.setOnCheckedChangeListener(Tainancheck);
        checkBox130.setOnCheckedChangeListener(Tainancheck);
        checkBox131.setOnCheckedChangeListener(Tainancheck);
        checkBox132.setOnCheckedChangeListener(Tainancheck);
        checkBox133.setOnCheckedChangeListener(Tainancheck);
        checkBox134.setOnCheckedChangeListener(Tainancheck);
        checkBox135.setOnCheckedChangeListener(Tainancheck);
        checkBox136.setOnCheckedChangeListener(Tainancheck);
        checkBox137.setOnCheckedChangeListener(Tainancheck);
        checkBox138.setOnCheckedChangeListener(Tainancheck);
        checkBox139.setOnCheckedChangeListener(Tainancheck);
        checkBox140.setOnCheckedChangeListener(Tainancheck);
        checkBox141.setOnCheckedChangeListener(Tainancheck);
        checkBox142.setOnCheckedChangeListener(Tainancheck);
        checkBox143.setOnCheckedChangeListener(Tainancheck);
        checkBox144.setOnCheckedChangeListener(Tainancheck);
        checkBox145.setOnCheckedChangeListener(Tainancheck);
        checkBox146.setOnCheckedChangeListener(Tainancheck);
        checkBox147.setOnCheckedChangeListener(Tainancheck);
        checkBox148.setOnCheckedChangeListener(Tainancheck);
        checkBox149.setOnCheckedChangeListener(Tainancheck);
        checkBox150.setOnCheckedChangeListener(Tainancheck);
        checkBox151.setOnCheckedChangeListener(Tainancheck);
        checkBox152.setOnCheckedChangeListener(Tainancheck);
        checkBox153.setOnCheckedChangeListener(Tainancheck);
        checkBox154.setOnCheckedChangeListener(Tainancheck);
        checkBox155.setOnCheckedChangeListener(Tainancheck);
        checkBox156.setOnCheckedChangeListener(Tainancheck);
        checkBox157.setOnCheckedChangeListener(Tainancheck);

        checkBox158.setOnCheckedChangeListener(Hualiencheck);
        checkBox159.setOnCheckedChangeListener(Hualiencheck);
        checkBox160.setOnCheckedChangeListener(Hualiencheck);
        checkBox161.setOnCheckedChangeListener(Hualiencheck);
        checkBox162.setOnCheckedChangeListener(Hualiencheck);
        checkBox163.setOnCheckedChangeListener(Hualiencheck);
        checkBox164.setOnCheckedChangeListener(Hualiencheck);
        checkBox165.setOnCheckedChangeListener(Hualiencheck);
        checkBox166.setOnCheckedChangeListener(Hualiencheck);
        checkBox167.setOnCheckedChangeListener(Hualiencheck);
        checkBox168.setOnCheckedChangeListener(Hualiencheck);
        checkBox169.setOnCheckedChangeListener(Hualiencheck);
        checkBox170.setOnCheckedChangeListener(Hualiencheck);

        checkBox171.setOnCheckedChangeListener(Miaocheck);
        checkBox172.setOnCheckedChangeListener(Miaocheck);
        checkBox173.setOnCheckedChangeListener(Miaocheck);
        checkBox174.setOnCheckedChangeListener(Miaocheck);
        checkBox175.setOnCheckedChangeListener(Miaocheck);
        checkBox176.setOnCheckedChangeListener(Miaocheck);
        checkBox177.setOnCheckedChangeListener(Miaocheck);
        checkBox178.setOnCheckedChangeListener(Miaocheck);
        checkBox179.setOnCheckedChangeListener(Miaocheck);
        checkBox180.setOnCheckedChangeListener(Miaocheck);
        checkBox181.setOnCheckedChangeListener(Miaocheck);
        checkBox182.setOnCheckedChangeListener(Miaocheck);
        checkBox183.setOnCheckedChangeListener(Miaocheck);
        checkBox184.setOnCheckedChangeListener(Miaocheck);
        checkBox185.setOnCheckedChangeListener(Miaocheck);
        checkBox186.setOnCheckedChangeListener(Miaocheck);
        checkBox187.setOnCheckedChangeListener(Miaocheck);
        checkBox188.setOnCheckedChangeListener(Miaocheck);

        checkBox189.setOnCheckedChangeListener(Taidongcheck);
        checkBox190.setOnCheckedChangeListener(Taidongcheck);
        checkBox191.setOnCheckedChangeListener(Taidongcheck);
        checkBox192.setOnCheckedChangeListener(Taidongcheck);
        checkBox193.setOnCheckedChangeListener(Taidongcheck);
        checkBox194.setOnCheckedChangeListener(Taidongcheck);
        checkBox195.setOnCheckedChangeListener(Taidongcheck);
        checkBox196.setOnCheckedChangeListener(Taidongcheck);
        checkBox197.setOnCheckedChangeListener(Taidongcheck);
        checkBox198.setOnCheckedChangeListener(Taidongcheck);
        checkBox199.setOnCheckedChangeListener(Taidongcheck);
        checkBox200.setOnCheckedChangeListener(Taidongcheck);
        checkBox201.setOnCheckedChangeListener(Taidongcheck);
        checkBox202.setOnCheckedChangeListener(Taidongcheck);
        checkBox203.setOnCheckedChangeListener(Taidongcheck);
        checkBox204.setOnCheckedChangeListener(Taidongcheck);

        checkBox205.setOnCheckedChangeListener(Pindongcheck);
        checkBox206.setOnCheckedChangeListener(Pindongcheck);
        checkBox207.setOnCheckedChangeListener(Pindongcheck);
        checkBox208.setOnCheckedChangeListener(Pindongcheck);
        checkBox209.setOnCheckedChangeListener(Pindongcheck);
        checkBox210.setOnCheckedChangeListener(Pindongcheck);
        checkBox211.setOnCheckedChangeListener(Pindongcheck);
        checkBox212.setOnCheckedChangeListener(Pindongcheck);
        checkBox213.setOnCheckedChangeListener(Pindongcheck);
        checkBox214.setOnCheckedChangeListener(Pindongcheck);
        checkBox215.setOnCheckedChangeListener(Pindongcheck);
        checkBox216.setOnCheckedChangeListener(Pindongcheck);
        checkBox217.setOnCheckedChangeListener(Pindongcheck);
        checkBox218.setOnCheckedChangeListener(Pindongcheck);
        checkBox219.setOnCheckedChangeListener(Pindongcheck);
        checkBox220.setOnCheckedChangeListener(Pindongcheck);
        checkBox221.setOnCheckedChangeListener(Pindongcheck);
        checkBox222.setOnCheckedChangeListener(Pindongcheck);
        checkBox223.setOnCheckedChangeListener(Pindongcheck);
        checkBox224.setOnCheckedChangeListener(Pindongcheck);
        checkBox225.setOnCheckedChangeListener(Pindongcheck);
        checkBox226.setOnCheckedChangeListener(Pindongcheck);
        checkBox227.setOnCheckedChangeListener(Pindongcheck);
        checkBox228.setOnCheckedChangeListener(Pindongcheck);
        checkBox229.setOnCheckedChangeListener(Pindongcheck);
        checkBox230.setOnCheckedChangeListener(Pindongcheck);
        checkBox231.setOnCheckedChangeListener(Pindongcheck);
        checkBox232.setOnCheckedChangeListener(Pindongcheck);
        checkBox233.setOnCheckedChangeListener(Pindongcheck);
        checkBox234.setOnCheckedChangeListener(Pindongcheck);
        checkBox235.setOnCheckedChangeListener(Pindongcheck);
        checkBox236.setOnCheckedChangeListener(Pindongcheck);

        checkBox237.setOnCheckedChangeListener(Shinchucountcheck);
        checkBox238.setOnCheckedChangeListener(Shinchucountcheck);
        checkBox239.setOnCheckedChangeListener(Shinchucountcheck);
        checkBox240.setOnCheckedChangeListener(Shinchucountcheck);
        checkBox241.setOnCheckedChangeListener(Shinchucountcheck);
        checkBox242.setOnCheckedChangeListener(Shinchucountcheck);
        checkBox243.setOnCheckedChangeListener(Shinchucountcheck);
        checkBox244.setOnCheckedChangeListener(Shinchucountcheck);
        checkBox245.setOnCheckedChangeListener(Shinchucountcheck);
        checkBox246.setOnCheckedChangeListener(Shinchucountcheck);
        checkBox247.setOnCheckedChangeListener(Shinchucountcheck);
        checkBox248.setOnCheckedChangeListener(Shinchucountcheck);
        checkBox249.setOnCheckedChangeListener(Shinchucountcheck);

        checkBox250.setOnCheckedChangeListener(Chunghuacheck);
        checkBox251.setOnCheckedChangeListener(Chunghuacheck);
        checkBox252.setOnCheckedChangeListener(Chunghuacheck);
        checkBox253.setOnCheckedChangeListener(Chunghuacheck);
        checkBox254.setOnCheckedChangeListener(Chunghuacheck);
        checkBox255.setOnCheckedChangeListener(Chunghuacheck);
        checkBox256.setOnCheckedChangeListener(Chunghuacheck);
        checkBox257.setOnCheckedChangeListener(Chunghuacheck);
        checkBox258.setOnCheckedChangeListener(Chunghuacheck);
        checkBox259.setOnCheckedChangeListener(Chunghuacheck);
        checkBox260.setOnCheckedChangeListener(Chunghuacheck);
        checkBox261.setOnCheckedChangeListener(Chunghuacheck);
        checkBox262.setOnCheckedChangeListener(Chunghuacheck);
        checkBox263.setOnCheckedChangeListener(Chunghuacheck);
        checkBox264.setOnCheckedChangeListener(Chunghuacheck);
        checkBox265.setOnCheckedChangeListener(Chunghuacheck);
        checkBox266.setOnCheckedChangeListener(Chunghuacheck);
        checkBox267.setOnCheckedChangeListener(Chunghuacheck);
        checkBox268.setOnCheckedChangeListener(Chunghuacheck);
        checkBox269.setOnCheckedChangeListener(Chunghuacheck);
        checkBox270.setOnCheckedChangeListener(Chunghuacheck);
        checkBox271.setOnCheckedChangeListener(Chunghuacheck);
        checkBox272.setOnCheckedChangeListener(Chunghuacheck);
        checkBox273.setOnCheckedChangeListener(Chunghuacheck);
        checkBox274.setOnCheckedChangeListener(Chunghuacheck);

        checkBox275.setOnCheckedChangeListener(Shinchucheck);
        checkBox276.setOnCheckedChangeListener(Shinchucheck);
        checkBox277.setOnCheckedChangeListener(Shinchucheck);

        checkBox268.setOnCheckedChangeListener(Nantoucheck);
        checkBox279.setOnCheckedChangeListener(Nantoucheck);
        checkBox280.setOnCheckedChangeListener(Nantoucheck);
        checkBox281.setOnCheckedChangeListener(Nantoucheck);
        checkBox282.setOnCheckedChangeListener(Nantoucheck);
        checkBox283.setOnCheckedChangeListener(Nantoucheck);
        checkBox284.setOnCheckedChangeListener(Nantoucheck);
        checkBox285.setOnCheckedChangeListener(Nantoucheck);
        checkBox286.setOnCheckedChangeListener(Nantoucheck);
        checkBox287.setOnCheckedChangeListener(Nantoucheck);
        checkBox288.setOnCheckedChangeListener(Nantoucheck);
        checkBox289.setOnCheckedChangeListener(Nantoucheck);
        checkBox290.setOnCheckedChangeListener(Nantoucheck);

        btn.setOnClickListener(btnClickListener);

        //切換到map取得目前位置
        btnDistance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, MapsActivity.class);

                Bundle bundle = new Bundle();
                bundle.putString("city","0");
                bundle.putString("area","0");

                intent.putExtras(bundle);
                //Log.e("key",city+" "+area);

                startActivity(intent);
                Toast.makeText(getApplicationContext(),"擷取資料中...",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private OnItemSelectedListener selectlistener = new OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            //int pos = spinner.
            checkBox1.setVisibility(View.INVISIBLE);
            checkBox2.setVisibility(View.INVISIBLE);
            checkBox3.setVisibility(View.INVISIBLE);
            checkBox4.setVisibility(View.INVISIBLE);
            checkBox5.setVisibility(View.INVISIBLE);
            checkBox6.setVisibility(View.INVISIBLE);
            checkBox7.setVisibility(View.INVISIBLE);
            checkBox8.setVisibility(View.INVISIBLE);
            checkBox9.setVisibility(View.INVISIBLE);
            checkBox10.setVisibility(View.INVISIBLE);
            checkBox11.setVisibility(View.INVISIBLE);
            checkBox12.setVisibility(View.INVISIBLE);

            checkBox13.setVisibility(View.INVISIBLE);
            checkBox14.setVisibility(View.INVISIBLE);
            checkBox15.setVisibility(View.INVISIBLE);
            checkBox16.setVisibility(View.INVISIBLE);
            checkBox17.setVisibility(View.INVISIBLE);
            checkBox18.setVisibility(View.INVISIBLE);
            checkBox19.setVisibility(View.INVISIBLE);
            checkBox20.setVisibility(View.INVISIBLE);
            checkBox21.setVisibility(View.INVISIBLE);
            checkBox22.setVisibility(View.INVISIBLE);
            checkBox23.setVisibility(View.INVISIBLE);
            checkBox24.setVisibility(View.INVISIBLE);
            checkBox25.setVisibility(View.INVISIBLE);
            checkBox26.setVisibility(View.INVISIBLE);
            checkBox27.setVisibility(View.INVISIBLE);
            checkBox28.setVisibility(View.INVISIBLE);
            checkBox29.setVisibility(View.INVISIBLE);
            checkBox30.setVisibility(View.INVISIBLE);
            checkBox31.setVisibility(View.INVISIBLE);
            checkBox32.setVisibility(View.INVISIBLE);
            checkBox33.setVisibility(View.INVISIBLE);
            checkBox34.setVisibility(View.INVISIBLE);
            checkBox35.setVisibility(View.INVISIBLE);
            checkBox36.setVisibility(View.INVISIBLE);
            checkBox37.setVisibility(View.INVISIBLE);
            checkBox38.setVisibility(View.INVISIBLE);
            checkBox39.setVisibility(View.INVISIBLE);
            checkBox40.setVisibility(View.INVISIBLE);
            checkBox41.setVisibility(View.INVISIBLE);

            checkBox42.setVisibility(View.INVISIBLE);
            checkBox43.setVisibility(View.INVISIBLE);
            checkBox44.setVisibility(View.INVISIBLE);
            checkBox45.setVisibility(View.INVISIBLE);
            checkBox46.setVisibility(View.INVISIBLE);
            checkBox47.setVisibility(View.INVISIBLE);
            checkBox48.setVisibility(View.INVISIBLE);
            checkBox49.setVisibility(View.INVISIBLE);
            checkBox57.setVisibility(View.INVISIBLE);
            checkBox58.setVisibility(View.INVISIBLE);

            checkBox50.setVisibility(View.INVISIBLE);
            checkBox51.setVisibility(View.INVISIBLE);
            checkBox52.setVisibility(View.INVISIBLE);
            checkBox53.setVisibility(View.INVISIBLE);
            checkBox54.setVisibility(View.INVISIBLE);
            checkBox55.setVisibility(View.INVISIBLE);
            checkBox56.setVisibility(View.INVISIBLE);

            checkBox59.setVisibility(View.INVISIBLE);
            checkBox60.setVisibility(View.INVISIBLE);
            checkBox61.setVisibility(View.INVISIBLE);
            checkBox62.setVisibility(View.INVISIBLE);
            checkBox63.setVisibility(View.INVISIBLE);
            checkBox64.setVisibility(View.INVISIBLE);
            checkBox65.setVisibility(View.INVISIBLE);
            checkBox66.setVisibility(View.INVISIBLE);
            checkBox67.setVisibility(View.INVISIBLE);
            checkBox68.setVisibility(View.INVISIBLE);
            checkBox69.setVisibility(View.INVISIBLE);
            checkBox70.setVisibility(View.INVISIBLE);
            checkBox71.setVisibility(View.INVISIBLE);

            checkBox72.setVisibility(View.INVISIBLE);
            checkBox73.setVisibility(View.INVISIBLE);
            checkBox74.setVisibility(View.INVISIBLE);
            checkBox75.setVisibility(View.INVISIBLE);
            checkBox76.setVisibility(View.INVISIBLE);
            checkBox77.setVisibility(View.INVISIBLE);
            checkBox78.setVisibility(View.INVISIBLE);
            checkBox79.setVisibility(View.INVISIBLE);
            checkBox80.setVisibility(View.INVISIBLE);
            checkBox81.setVisibility(View.INVISIBLE);
            checkBox82.setVisibility(View.INVISIBLE);
            checkBox83.setVisibility(View.INVISIBLE);
            checkBox84.setVisibility(View.INVISIBLE);
            checkBox85.setVisibility(View.INVISIBLE);
            checkBox86.setVisibility(View.INVISIBLE);
            checkBox87.setVisibility(View.INVISIBLE);
            checkBox88.setVisibility(View.INVISIBLE);
            checkBox89.setVisibility(View.INVISIBLE);
            checkBox90.setVisibility(View.INVISIBLE);
            checkBox91.setVisibility(View.INVISIBLE);
            checkBox92.setVisibility(View.INVISIBLE);
            checkBox93.setVisibility(View.INVISIBLE);
            checkBox94.setVisibility(View.INVISIBLE);
            checkBox95.setVisibility(View.INVISIBLE);
            checkBox96.setVisibility(View.INVISIBLE);
            checkBox97.setVisibility(View.INVISIBLE);
            checkBox98.setVisibility(View.INVISIBLE);
            checkBox99.setVisibility(View.INVISIBLE);
            checkBox100.setVisibility(View.INVISIBLE);

            checkBox101.setVisibility(View.INVISIBLE);
            checkBox102.setVisibility(View.INVISIBLE);
            checkBox103.setVisibility(View.INVISIBLE);
            checkBox104.setVisibility(View.INVISIBLE);
            checkBox105.setVisibility(View.INVISIBLE);
            checkBox106.setVisibility(View.INVISIBLE);
            checkBox107.setVisibility(View.INVISIBLE);
            checkBox108.setVisibility(View.INVISIBLE);
            checkBox109.setVisibility(View.INVISIBLE);
            checkBox110.setVisibility(View.INVISIBLE);
            checkBox111.setVisibility(View.INVISIBLE);
            checkBox112.setVisibility(View.INVISIBLE);
            checkBox113.setVisibility(View.INVISIBLE);
            checkBox114.setVisibility(View.INVISIBLE);
            checkBox115.setVisibility(View.INVISIBLE);
            checkBox116.setVisibility(View.INVISIBLE);
            checkBox117.setVisibility(View.INVISIBLE);
            checkBox118.setVisibility(View.INVISIBLE);
            checkBox119.setVisibility(View.INVISIBLE);
            checkBox120.setVisibility(View.INVISIBLE);

            checkBox121.setVisibility(View.INVISIBLE);
            checkBox122.setVisibility(View.INVISIBLE);
            checkBox123.setVisibility(View.INVISIBLE);
            checkBox124.setVisibility(View.INVISIBLE);
            checkBox125.setVisibility(View.INVISIBLE);
            checkBox126.setVisibility(View.INVISIBLE);
            checkBox127.setVisibility(View.INVISIBLE);
            checkBox128.setVisibility(View.INVISIBLE);
            checkBox129.setVisibility(View.INVISIBLE);
            checkBox130.setVisibility(View.INVISIBLE);
            checkBox131.setVisibility(View.INVISIBLE);
            checkBox132.setVisibility(View.INVISIBLE);
            checkBox133.setVisibility(View.INVISIBLE);
            checkBox134.setVisibility(View.INVISIBLE);
            checkBox135.setVisibility(View.INVISIBLE);
            checkBox136.setVisibility(View.INVISIBLE);
            checkBox137.setVisibility(View.INVISIBLE);
            checkBox138.setVisibility(View.INVISIBLE);
            checkBox139.setVisibility(View.INVISIBLE);
            checkBox140.setVisibility(View.INVISIBLE);
            checkBox141.setVisibility(View.INVISIBLE);
            checkBox142.setVisibility(View.INVISIBLE);
            checkBox143.setVisibility(View.INVISIBLE);
            checkBox144.setVisibility(View.INVISIBLE);
            checkBox145.setVisibility(View.INVISIBLE);
            checkBox146.setVisibility(View.INVISIBLE);
            checkBox147.setVisibility(View.INVISIBLE);
            checkBox148.setVisibility(View.INVISIBLE);
            checkBox149.setVisibility(View.INVISIBLE);
            checkBox150.setVisibility(View.INVISIBLE);
            checkBox151.setVisibility(View.INVISIBLE);
            checkBox152.setVisibility(View.INVISIBLE);
            checkBox153.setVisibility(View.INVISIBLE);
            checkBox154.setVisibility(View.INVISIBLE);
            checkBox155.setVisibility(View.INVISIBLE);
            checkBox156.setVisibility(View.INVISIBLE);
            checkBox157.setVisibility(View.INVISIBLE);

            checkBox158.setVisibility(View.INVISIBLE);
            checkBox159.setVisibility(View.INVISIBLE);
            checkBox160.setVisibility(View.INVISIBLE);
            checkBox161.setVisibility(View.INVISIBLE);
            checkBox162.setVisibility(View.INVISIBLE);
            checkBox163.setVisibility(View.INVISIBLE);
            checkBox164.setVisibility(View.INVISIBLE);
            checkBox165.setVisibility(View.INVISIBLE);
            checkBox166.setVisibility(View.INVISIBLE);
            checkBox167.setVisibility(View.INVISIBLE);
            checkBox168.setVisibility(View.INVISIBLE);
            checkBox169.setVisibility(View.INVISIBLE);
            checkBox170.setVisibility(View.INVISIBLE);

            checkBox171.setVisibility(View.INVISIBLE);
            checkBox172.setVisibility(View.INVISIBLE);
            checkBox173.setVisibility(View.INVISIBLE);
            checkBox174.setVisibility(View.INVISIBLE);
            checkBox175.setVisibility(View.INVISIBLE);
            checkBox176.setVisibility(View.INVISIBLE);
            checkBox177.setVisibility(View.INVISIBLE);
            checkBox178.setVisibility(View.INVISIBLE);
            checkBox179.setVisibility(View.INVISIBLE);
            checkBox180.setVisibility(View.INVISIBLE);
            checkBox181.setVisibility(View.INVISIBLE);
            checkBox182.setVisibility(View.INVISIBLE);
            checkBox183.setVisibility(View.INVISIBLE);
            checkBox184.setVisibility(View.INVISIBLE);
            checkBox185.setVisibility(View.INVISIBLE);
            checkBox186.setVisibility(View.INVISIBLE);
            checkBox187.setVisibility(View.INVISIBLE);
            checkBox188.setVisibility(View.INVISIBLE);

            checkBox189.setVisibility(View.INVISIBLE);
            checkBox190.setVisibility(View.INVISIBLE);
            checkBox191.setVisibility(View.INVISIBLE);
            checkBox192.setVisibility(View.INVISIBLE);
            checkBox193.setVisibility(View.INVISIBLE);
            checkBox194.setVisibility(View.INVISIBLE);
            checkBox195.setVisibility(View.INVISIBLE);
            checkBox196.setVisibility(View.INVISIBLE);
            checkBox197.setVisibility(View.INVISIBLE);
            checkBox198.setVisibility(View.INVISIBLE);
            checkBox199.setVisibility(View.INVISIBLE);
            checkBox200.setVisibility(View.INVISIBLE);
            checkBox201.setVisibility(View.INVISIBLE);
            checkBox202.setVisibility(View.INVISIBLE);
            checkBox203.setVisibility(View.INVISIBLE);
            checkBox204.setVisibility(View.INVISIBLE);

            checkBox205.setVisibility(View.INVISIBLE);
            checkBox206.setVisibility(View.INVISIBLE);
            checkBox207.setVisibility(View.INVISIBLE);
            checkBox208.setVisibility(View.INVISIBLE);
            checkBox209.setVisibility(View.INVISIBLE);
            checkBox210.setVisibility(View.INVISIBLE);
            checkBox211.setVisibility(View.INVISIBLE);
            checkBox212.setVisibility(View.INVISIBLE);
            checkBox213.setVisibility(View.INVISIBLE);
            checkBox214.setVisibility(View.INVISIBLE);
            checkBox215.setVisibility(View.INVISIBLE);
            checkBox216.setVisibility(View.INVISIBLE);
            checkBox217.setVisibility(View.INVISIBLE);
            checkBox218.setVisibility(View.INVISIBLE);
            checkBox219.setVisibility(View.INVISIBLE);
            checkBox220.setVisibility(View.INVISIBLE);
            checkBox221.setVisibility(View.INVISIBLE);
            checkBox222.setVisibility(View.INVISIBLE);
            checkBox223.setVisibility(View.INVISIBLE);
            checkBox224.setVisibility(View.INVISIBLE);
            checkBox225.setVisibility(View.INVISIBLE);
            checkBox226.setVisibility(View.INVISIBLE);
            checkBox227.setVisibility(View.INVISIBLE);
            checkBox228.setVisibility(View.INVISIBLE);
            checkBox229.setVisibility(View.INVISIBLE);
            checkBox230.setVisibility(View.INVISIBLE);
            checkBox231.setVisibility(View.INVISIBLE);
            checkBox232.setVisibility(View.INVISIBLE);
            checkBox233.setVisibility(View.INVISIBLE);
            checkBox234.setVisibility(View.INVISIBLE);
            checkBox235.setVisibility(View.INVISIBLE);
            checkBox236.setVisibility(View.INVISIBLE);

            checkBox237.setVisibility(View.INVISIBLE);
            checkBox238.setVisibility(View.INVISIBLE);
            checkBox239.setVisibility(View.INVISIBLE);
            checkBox240.setVisibility(View.INVISIBLE);
            checkBox241.setVisibility(View.INVISIBLE);
            checkBox242.setVisibility(View.INVISIBLE);
            checkBox243.setVisibility(View.INVISIBLE);
            checkBox244.setVisibility(View.INVISIBLE);
            checkBox245.setVisibility(View.INVISIBLE);
            checkBox246.setVisibility(View.INVISIBLE);
            checkBox247.setVisibility(View.INVISIBLE);
            checkBox248.setVisibility(View.INVISIBLE);
            checkBox249.setVisibility(View.INVISIBLE);

            checkBox250.setVisibility(View.INVISIBLE);
            checkBox251.setVisibility(View.INVISIBLE);
            checkBox252.setVisibility(View.INVISIBLE);
            checkBox253.setVisibility(View.INVISIBLE);
            checkBox254.setVisibility(View.INVISIBLE);
            checkBox255.setVisibility(View.INVISIBLE);
            checkBox256.setVisibility(View.INVISIBLE);
            checkBox257.setVisibility(View.INVISIBLE);
            checkBox258.setVisibility(View.INVISIBLE);
            checkBox259.setVisibility(View.INVISIBLE);
            checkBox260.setVisibility(View.INVISIBLE);
            checkBox261.setVisibility(View.INVISIBLE);
            checkBox262.setVisibility(View.INVISIBLE);
            checkBox263.setVisibility(View.INVISIBLE);
            checkBox264.setVisibility(View.INVISIBLE);
            checkBox265.setVisibility(View.INVISIBLE);
            checkBox266.setVisibility(View.INVISIBLE);
            checkBox267.setVisibility(View.INVISIBLE);
            checkBox268.setVisibility(View.INVISIBLE);
            checkBox269.setVisibility(View.INVISIBLE);
            checkBox270.setVisibility(View.INVISIBLE);
            checkBox271.setVisibility(View.INVISIBLE);
            checkBox272.setVisibility(View.INVISIBLE);
            checkBox273.setVisibility(View.INVISIBLE);
            checkBox274.setVisibility(View.INVISIBLE);

            checkBox275.setVisibility(View.INVISIBLE);
            checkBox276.setVisibility(View.INVISIBLE);
            checkBox277.setVisibility(View.INVISIBLE);

            checkBox278.setVisibility(View.INVISIBLE);
            checkBox279.setVisibility(View.INVISIBLE);
            checkBox280.setVisibility(View.INVISIBLE);
            checkBox281.setVisibility(View.INVISIBLE);
            checkBox282.setVisibility(View.INVISIBLE);
            checkBox283.setVisibility(View.INVISIBLE);
            checkBox284.setVisibility(View.INVISIBLE);
            checkBox285.setVisibility(View.INVISIBLE);
            checkBox286.setVisibility(View.INVISIBLE);
            checkBox287.setVisibility(View.INVISIBLE);
            checkBox288.setVisibility(View.INVISIBLE);
            checkBox289.setVisibility(View.INVISIBLE);
            checkBox290.setVisibility(View.INVISIBLE);

            if (i == 1) {//台北
                city = "taipei";
                checkBox1.setVisibility(VISIBLE);
                checkBox2.setVisibility(VISIBLE);
                checkBox3.setVisibility(VISIBLE);
                checkBox4.setVisibility(VISIBLE);
                checkBox5.setVisibility(VISIBLE);
                checkBox6.setVisibility(VISIBLE);
                checkBox7.setVisibility(VISIBLE);
                checkBox8.setVisibility(VISIBLE);
                checkBox9.setVisibility(VISIBLE);
                checkBox10.setVisibility(VISIBLE);
                checkBox11.setVisibility(VISIBLE);
                checkBox12.setVisibility(VISIBLE);

            }

            if (i == 2) {//新北
                city = "shinpei";

                checkBox13.setVisibility(VISIBLE);
                checkBox14.setVisibility(VISIBLE);
                checkBox15.setVisibility(VISIBLE);
                checkBox16.setVisibility(VISIBLE);
                checkBox17.setVisibility(VISIBLE);
                checkBox18.setVisibility(VISIBLE);
                checkBox19.setVisibility(VISIBLE);
                checkBox20.setVisibility(VISIBLE);
                checkBox21.setVisibility(VISIBLE);
                checkBox22.setVisibility(VISIBLE);
                checkBox23.setVisibility(VISIBLE);
                checkBox24.setVisibility(VISIBLE);
                checkBox25.setVisibility(VISIBLE);
                checkBox26.setVisibility(VISIBLE);
                checkBox27.setVisibility(VISIBLE);
                checkBox28.setVisibility(VISIBLE);
                checkBox29.setVisibility(VISIBLE);
                checkBox30.setVisibility(VISIBLE);
                checkBox31.setVisibility(VISIBLE);
                checkBox32.setVisibility(VISIBLE);
                checkBox33.setVisibility(VISIBLE);
                checkBox34.setVisibility(VISIBLE);
                checkBox35.setVisibility(VISIBLE);
                checkBox36.setVisibility(VISIBLE);
                checkBox37.setVisibility(VISIBLE);
                checkBox38.setVisibility(VISIBLE);
                checkBox39.setVisibility(VISIBLE);
                checkBox40.setVisibility(VISIBLE);
                checkBox41.setVisibility(VISIBLE);


            }

            if(i == 3)//基隆
            {

                city = "keelung";


                checkBox50.setVisibility(VISIBLE);
                checkBox51.setVisibility(VISIBLE);
                checkBox52.setVisibility(VISIBLE);
                checkBox53.setVisibility(VISIBLE);
                checkBox54.setVisibility(VISIBLE);
                checkBox55.setVisibility(VISIBLE);
                checkBox56.setVisibility(VISIBLE);



            }
            if(i == 4)
            {//新竹市
                city = "hsinchu";
                checkBox275.setVisibility(VISIBLE);
                checkBox276.setVisibility(VISIBLE);
                checkBox277.setVisibility(VISIBLE);
            }
            if(i == 5)
            {//桃園

                city = "taoyuan";

                checkBox59.setVisibility(VISIBLE);
                checkBox60.setVisibility(VISIBLE);
                checkBox61.setVisibility(VISIBLE);
                checkBox62.setVisibility(VISIBLE);
                checkBox63.setVisibility(VISIBLE);
                checkBox64.setVisibility(VISIBLE);
                checkBox65.setVisibility(VISIBLE);
                checkBox66.setVisibility(VISIBLE);
                checkBox67.setVisibility(VISIBLE);
                checkBox68.setVisibility(VISIBLE);
                checkBox69.setVisibility(VISIBLE);
                checkBox70.setVisibility(VISIBLE);
                checkBox71.setVisibility(VISIBLE);



            }

            if (i == 6) {//雲林
                city = "yunlin";

                checkBox101.setVisibility(View.VISIBLE);
                checkBox102.setVisibility(View.VISIBLE);
                checkBox103.setVisibility(View.VISIBLE);
                checkBox104.setVisibility(View.VISIBLE);
                checkBox105.setVisibility(View.VISIBLE);
                checkBox106.setVisibility(View.VISIBLE);
                checkBox107.setVisibility(View.VISIBLE);
                checkBox108.setVisibility(View.VISIBLE);
                checkBox109.setVisibility(View.VISIBLE);
                checkBox110.setVisibility(View.VISIBLE);
                checkBox111.setVisibility(View.VISIBLE);
                checkBox112.setVisibility(View.VISIBLE);
                checkBox113.setVisibility(View.VISIBLE);
                checkBox114.setVisibility(View.VISIBLE);
                checkBox115.setVisibility(View.VISIBLE);
                checkBox116.setVisibility(View.VISIBLE);
                checkBox117.setVisibility(View.VISIBLE);
                checkBox118.setVisibility(View.VISIBLE);
                checkBox119.setVisibility(View.VISIBLE);
                checkBox120.setVisibility(View.VISIBLE);


            }
            if (i == 7) {//台中
                city = "taichung";


                checkBox72.setVisibility(VISIBLE);
                checkBox73.setVisibility(VISIBLE);
                checkBox74.setVisibility(VISIBLE);
                checkBox75.setVisibility(VISIBLE);
                checkBox76.setVisibility(VISIBLE);
                checkBox77.setVisibility(VISIBLE);
                checkBox78.setVisibility(VISIBLE);
                checkBox79.setVisibility(VISIBLE);
                checkBox80.setVisibility(VISIBLE);
                checkBox81.setVisibility(VISIBLE);
                checkBox82.setVisibility(VISIBLE);
                checkBox83.setVisibility(VISIBLE);
                checkBox84.setVisibility(VISIBLE);
                checkBox85.setVisibility(VISIBLE);
                checkBox86.setVisibility(VISIBLE);
                checkBox87.setVisibility(VISIBLE);
                checkBox88.setVisibility(VISIBLE);
                checkBox89.setVisibility(VISIBLE);
                checkBox90.setVisibility(VISIBLE);
                checkBox91.setVisibility(VISIBLE);
                checkBox92.setVisibility(VISIBLE);
                checkBox93.setVisibility(VISIBLE);
                checkBox94.setVisibility(VISIBLE);
                checkBox95.setVisibility(VISIBLE);
                checkBox96.setVisibility(VISIBLE);
                checkBox97.setVisibility(VISIBLE);
                checkBox98.setVisibility(VISIBLE);
                checkBox99.setVisibility(VISIBLE);
                checkBox100.setVisibility(VISIBLE);


            }
            if(i == 8)
            {//南投縣
                city = "nantou";

                checkBox278.setVisibility(VISIBLE);
                checkBox279.setVisibility(VISIBLE);
                checkBox280.setVisibility(VISIBLE);
                checkBox281.setVisibility(VISIBLE);
                checkBox282.setVisibility(VISIBLE);
                checkBox283.setVisibility(VISIBLE);
                checkBox284.setVisibility(VISIBLE);
                checkBox285.setVisibility(VISIBLE);
                checkBox286.setVisibility(VISIBLE);
                checkBox287.setVisibility(VISIBLE);
                checkBox288.setVisibility(VISIBLE);
                checkBox289.setVisibility(VISIBLE);
                checkBox290.setVisibility(VISIBLE);
            }
            if(i == 9)
            {//高雄
                city = "kaohsiung";

                checkBox42.setVisibility(VISIBLE);
                checkBox43.setVisibility(VISIBLE);
                checkBox44.setVisibility(VISIBLE);
                checkBox45.setVisibility(VISIBLE);
                checkBox46.setVisibility(VISIBLE);
                checkBox47.setVisibility(VISIBLE);
                checkBox48.setVisibility(VISIBLE);
                checkBox49.setVisibility(VISIBLE);
                checkBox57.setVisibility(VISIBLE);
                checkBox58.setVisibility(VISIBLE);


            }
            if(i == 10)
            {//台南
                city = "tainan";

                checkBox121.setVisibility(VISIBLE);
                checkBox122.setVisibility(VISIBLE);
                checkBox123.setVisibility(VISIBLE);
                checkBox124.setVisibility(VISIBLE);
                checkBox125.setVisibility(VISIBLE);
                checkBox126.setVisibility(VISIBLE);
                checkBox127.setVisibility(VISIBLE);
                checkBox128.setVisibility(VISIBLE);
                checkBox129.setVisibility(VISIBLE);
                checkBox130.setVisibility(VISIBLE);
                checkBox131.setVisibility(VISIBLE);
                checkBox132.setVisibility(VISIBLE);
                checkBox133.setVisibility(VISIBLE);
                checkBox134.setVisibility(VISIBLE);
                checkBox135.setVisibility(VISIBLE);
                checkBox136.setVisibility(VISIBLE);
                checkBox137.setVisibility(VISIBLE);
                checkBox138.setVisibility(VISIBLE);
                checkBox139.setVisibility(VISIBLE);
                checkBox140.setVisibility(VISIBLE);
                checkBox141.setVisibility(VISIBLE);
                checkBox142.setVisibility(VISIBLE);
                checkBox143.setVisibility(VISIBLE);
                checkBox144.setVisibility(VISIBLE);
                checkBox145.setVisibility(VISIBLE);
                checkBox146.setVisibility(VISIBLE);
                checkBox147.setVisibility(VISIBLE);
                checkBox148.setVisibility(VISIBLE);
                checkBox149.setVisibility(VISIBLE);
                checkBox150.setVisibility(VISIBLE);
                checkBox151.setVisibility(VISIBLE);
                checkBox152.setVisibility(VISIBLE);
                checkBox153.setVisibility(VISIBLE);
                checkBox154.setVisibility(VISIBLE);
                checkBox155.setVisibility(VISIBLE);
                checkBox156.setVisibility(VISIBLE);
                checkBox157.setVisibility(VISIBLE);


            }
            if(i == 11)
            {//花蓮
                city = "hualien";

                checkBox158.setVisibility(VISIBLE);
                checkBox159.setVisibility(VISIBLE);
                checkBox160.setVisibility(VISIBLE);
                checkBox161.setVisibility(VISIBLE);
                checkBox162.setVisibility(VISIBLE);
                checkBox163.setVisibility(VISIBLE);
                checkBox164.setVisibility(VISIBLE);
                checkBox165.setVisibility(VISIBLE);
                checkBox166.setVisibility(VISIBLE);
                checkBox167.setVisibility(VISIBLE);
                checkBox168.setVisibility(VISIBLE);
                checkBox169.setVisibility(VISIBLE);
                checkBox170.setVisibility(VISIBLE);


            }
            if(i == 12)
            {//苗栗
                city = "miaoli";


                checkBox171.setVisibility(VISIBLE);
                checkBox172.setVisibility(VISIBLE);
                checkBox173.setVisibility(VISIBLE);
                checkBox174.setVisibility(VISIBLE);
                checkBox175.setVisibility(VISIBLE);
                checkBox176.setVisibility(VISIBLE);
                checkBox177.setVisibility(VISIBLE);
                checkBox178.setVisibility(VISIBLE);
                checkBox179.setVisibility(VISIBLE);
                checkBox180.setVisibility(VISIBLE);
                checkBox181.setVisibility(VISIBLE);
                checkBox182.setVisibility(VISIBLE);
                checkBox183.setVisibility(VISIBLE);
                checkBox184.setVisibility(VISIBLE);
                checkBox185.setVisibility(VISIBLE);
                checkBox186.setVisibility(VISIBLE);
                checkBox187.setVisibility(VISIBLE);
                checkBox188.setVisibility(VISIBLE);



            }
            if(i == 13)
            {//台東
                city = "taitung";


                checkBox189.setVisibility(VISIBLE);
                checkBox190.setVisibility(VISIBLE);
                checkBox191.setVisibility(VISIBLE);
                checkBox192.setVisibility(VISIBLE);
                checkBox193.setVisibility(VISIBLE);
                checkBox194.setVisibility(VISIBLE);
                checkBox195.setVisibility(VISIBLE);
                checkBox196.setVisibility(VISIBLE);
                checkBox197.setVisibility(VISIBLE);
                checkBox198.setVisibility(VISIBLE);
                checkBox199.setVisibility(VISIBLE);
                checkBox200.setVisibility(VISIBLE);
                checkBox201.setVisibility(VISIBLE);
                checkBox202.setVisibility(VISIBLE);
                checkBox203.setVisibility(VISIBLE);
                checkBox204.setVisibility(VISIBLE);


            }
            if(i == 14)
            {//屏東
                city = "pintung";


                checkBox205.setVisibility(VISIBLE);
                checkBox206.setVisibility(VISIBLE);
                checkBox207.setVisibility(VISIBLE);
                checkBox208.setVisibility(VISIBLE);
                checkBox209.setVisibility(VISIBLE);
                checkBox210.setVisibility(VISIBLE);
                checkBox211.setVisibility(VISIBLE);
                checkBox212.setVisibility(VISIBLE);
                checkBox213.setVisibility(VISIBLE);
                checkBox214.setVisibility(VISIBLE);
                checkBox215.setVisibility(VISIBLE);
                checkBox216.setVisibility(VISIBLE);
                checkBox217.setVisibility(VISIBLE);
                checkBox218.setVisibility(VISIBLE);
                checkBox219.setVisibility(VISIBLE);
                checkBox220.setVisibility(VISIBLE);
                checkBox221.setVisibility(VISIBLE);
                checkBox222.setVisibility(VISIBLE);
                checkBox223.setVisibility(VISIBLE);
                checkBox224.setVisibility(VISIBLE);
                checkBox225.setVisibility(VISIBLE);
                checkBox226.setVisibility(VISIBLE);
                checkBox227.setVisibility(VISIBLE);
                checkBox228.setVisibility(VISIBLE);
                checkBox229.setVisibility(VISIBLE);
                checkBox230.setVisibility(VISIBLE);
                checkBox231.setVisibility(VISIBLE);
                checkBox232.setVisibility(VISIBLE);
                checkBox233.setVisibility(VISIBLE);
                checkBox234.setVisibility(VISIBLE);
                checkBox235.setVisibility(VISIBLE);
                checkBox236.setVisibility(VISIBLE);


            }
            if(i == 15)
            {//新竹縣
                city = "hsinchucounty";


                checkBox237.setVisibility(VISIBLE);
                checkBox238.setVisibility(VISIBLE);
                checkBox239.setVisibility(VISIBLE);
                checkBox240.setVisibility(VISIBLE);
                checkBox241.setVisibility(VISIBLE);
                checkBox242.setVisibility(VISIBLE);
                checkBox243.setVisibility(VISIBLE);
                checkBox244.setVisibility(VISIBLE);
                checkBox245.setVisibility(VISIBLE);
                checkBox246.setVisibility(VISIBLE);
                checkBox247.setVisibility(VISIBLE);
                checkBox248.setVisibility(VISIBLE);
                checkBox249.setVisibility(VISIBLE);

            }
            if(i == 16)
            {//彰化縣
                city = "chuhua";

                checkBox250.setVisibility(VISIBLE);
                checkBox251.setVisibility(VISIBLE);
                checkBox252.setVisibility(VISIBLE);
                checkBox253.setVisibility(VISIBLE);
                checkBox254.setVisibility(VISIBLE);
                checkBox255.setVisibility(VISIBLE);
                checkBox256.setVisibility(VISIBLE);
                checkBox257.setVisibility(VISIBLE);
                checkBox258.setVisibility(VISIBLE);
                checkBox259.setVisibility(VISIBLE);
                checkBox260.setVisibility(VISIBLE);
                checkBox261.setVisibility(VISIBLE);
                checkBox262.setVisibility(VISIBLE);
                checkBox263.setVisibility(VISIBLE);
                checkBox264.setVisibility(VISIBLE);
                checkBox265.setVisibility(VISIBLE);
                checkBox266.setVisibility(VISIBLE);
                checkBox267.setVisibility(VISIBLE);
                checkBox268.setVisibility(VISIBLE);
                checkBox269.setVisibility(VISIBLE);
                checkBox270.setVisibility(VISIBLE);
                checkBox271.setVisibility(VISIBLE);
                checkBox272.setVisibility(VISIBLE);
                checkBox273.setVisibility(VISIBLE);
                checkBox274.setVisibility(VISIBLE);

            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> spin) {

        }
    };

    private CheckBox.OnCheckedChangeListener Taicheck = new CheckBox.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            if (checkBox1.isChecked())
                area = "南港區";
            else if (checkBox2.isChecked())
                area = "大安區";
            else if (checkBox3.isChecked())
                area = "信義區";
            else if (checkBox4.isChecked())
                area = "松山區";
            else if (checkBox5.isChecked())
                area = "中山區";
            else if (checkBox6.isChecked())
                area = "大同區";
            else if (checkBox7.isChecked())
                area = "文山區";
            else if (checkBox8.isChecked())
                area = "中正區";
            else if (checkBox9.isChecked())
                area = "萬華區";
            else if (checkBox10.isChecked())
                area = "士林區";
            else if (checkBox11.isChecked())
                area = "北投區";
            else if (checkBox12.isChecked())
                area = "內湖區";
        }
    };
    private CheckBox.OnCheckedChangeListener Shincheck = new CheckBox.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            if (checkBox13.isChecked())
                area = "新莊區";
            else if (checkBox14.isChecked())
                area = "永和區";
            else if (checkBox15.isChecked())
                area = "中和區";
            else if (checkBox16.isChecked())
                area = "三重區";
            else if (checkBox17.isChecked())
                area = "新店區";
            else if (checkBox18.isChecked())
                area = "土城區";
            else if (checkBox19.isChecked())
                area = "蘆洲區";
            else if (checkBox20.isChecked())
                area = "樹林區";
            else if (checkBox21.isChecked())
                area = "淡水區";
            else if (checkBox22.isChecked())
                area = "三峽區";
            else if (checkBox23.isChecked())
                area = "林口區";
            else if (checkBox24.isChecked())
                area = "鶯歌區";
            else if (checkBox25.isChecked())
                area = "五股區";
            else if (checkBox26.isChecked())
                area = "泰山區";
            else if (checkBox27.isChecked())
                area = "瑞芳區";
            else if (checkBox28.isChecked())
                area = "八里區";
            else if (checkBox29.isChecked())
                area = "深坑區";
            else if (checkBox30.isChecked())
                area = "三芝區";
            else if (checkBox31.isChecked())
                area = "萬里區";
            else if (checkBox32.isChecked())
                area = "金山區";
            else if (checkBox33.isChecked())
                area = "貢寮區";
            else if (checkBox34.isChecked())
                area = "石門區";
            else if (checkBox35.isChecked())
                area = "雙溪區";
            else if (checkBox36.isChecked())
                area = "平溪區";
            else if (checkBox37.isChecked())
                area = "石碇區";
            else if (checkBox38.isChecked())
                area = "坪林區";
            else if (checkBox39.isChecked())
                area = "烏來區";
            else if (checkBox40.isChecked())
                area = "板橋區";
            else if (checkBox41.isChecked())
                area = "汐止區";
        }
    };
    private CheckBox.OnCheckedChangeListener Kaocheck = new CheckBox.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            if (checkBox42.isChecked())
                area = "永安區";
            else if (checkBox43.isChecked())
                area = "湖內區";
            else if (checkBox44.isChecked())
                area = "路竹區";
            else if (checkBox45.isChecked())
                area = "仁武區";
            else if (checkBox46.isChecked())
                area = "大社區";
            else if (checkBox47.isChecked())
                area = "大樹區";
            else if (checkBox48.isChecked())
                area = "小港區";
            else if (checkBox49.isChecked())
                area = "旗津區";
            else if (checkBox57.isChecked())
                area = "前鎮區";
            else if (checkBox58.isChecked())
                area = "苓雅區";
        }
    };
    private CheckBox.OnCheckedChangeListener Jicheck = new CheckBox.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            if (checkBox50.isChecked())
                area = "信義區";
            else if (checkBox51.isChecked())
                area = "中正區";
            else if (checkBox52.isChecked())
                area = "安樂區";
            else if (checkBox53.isChecked())
                area = "中山區";
            else if (checkBox54.isChecked())
                area = "仁愛區";
            else if (checkBox55.isChecked())
                area = "暖暖區";
            else if (checkBox56.isChecked())
                area = "七堵區";
        }
    };

    private CheckBox.OnCheckedChangeListener Taocheck = new CheckBox.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            if (checkBox59.isChecked())
                area = "桃園區";
            else if (checkBox60.isChecked())
                area = "中壢區";
            else if (checkBox61.isChecked())
                area = "大溪區";
            else if (checkBox62.isChecked())
                area = "楊梅區";
            else if (checkBox63.isChecked())
                area = "蘆竹區";
            else if (checkBox64.isChecked())
                area = "大園區";
            else if (checkBox65.isChecked())
                area = "龜山區";
            else if (checkBox66.isChecked())
                area = "八德區";
            else if (checkBox67.isChecked())
                area = "龍潭區";
            else if (checkBox68.isChecked())
                area = "平正區";
            else if (checkBox69.isChecked())
                area = "新屋區";
            else if (checkBox70.isChecked())
                area = "觀音區";
            else if (checkBox71.isChecked())
                area = "復興區";
        }
    };
    private CheckBox.OnCheckedChangeListener Taichungcheck = new CheckBox.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            if (checkBox72.isChecked())
                area = "大安區";
            else if (checkBox73.isChecked())
                area = "豐原區";
            else if (checkBox74.isChecked())
                area = "東勢區";
            else if (checkBox75.isChecked())
                area = "大甲區";
            else if (checkBox76.isChecked())
                area = "清水區";
            else if (checkBox77.isChecked())
                area= "沙路區";
            else if (checkBox78.isChecked())
                area = "梧棲區";
            else if (checkBox79.isChecked())
                area = "后里區";
            else if (checkBox80.isChecked())
                area = "神岡區";
            else if (checkBox81.isChecked())
                area = "潭子區";
            else if (checkBox82.isChecked())
                area = "大雅區";
            else if (checkBox83.isChecked())
                area = "新社區";
            else if (checkBox84.isChecked())
                area = "石岡區";
            else if (checkBox85.isChecked())
                area = "外埔區";
            else if (checkBox86.isChecked())
                area = "烏日區";
            else if (checkBox87.isChecked())
                area = "大肚區";
            else if (checkBox88.isChecked())
                area = "龍井區";
            else if (checkBox89.isChecked())
                area = "霧峰區";
            else if (checkBox90.isChecked())
                area = "太平區";
            else if (checkBox91.isChecked())
                area = "大里區";
            else if (checkBox92.isChecked())
                area = "和平區";
            else if (checkBox93.isChecked())
                area = "東區";
            else if (checkBox94.isChecked())
                area = "西區";
            else if (checkBox95.isChecked())
                area = "南屯區";
            else if (checkBox96.isChecked())
                area = "北屯區";
            else if (checkBox97.isChecked())
                area = "西屯區";
            else if (checkBox98.isChecked())
                area = "南區";
            else if (checkBox99.isChecked())
                area = "北區";
            else if (checkBox100.isChecked())
                area = "中區";
        }
    };
    private CheckBox.OnCheckedChangeListener Yuncheck = new CheckBox.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            if (checkBox101.isChecked())
                area = "斗南鎮";
            else if (checkBox102.isChecked())
                area = "斗六市";
            else if (checkBox103.isChecked())
                area = "虎尾鎮";
            else if (checkBox104.isChecked())
                area = "西螺鎮";
            else if (checkBox105.isChecked())
                area = "土庫鎮";
            else if (checkBox106.isChecked())
                area = "北港鎮";
            else if (checkBox107.isChecked())
                area = "古坑鄉";
            else if (checkBox108.isChecked())
                area = "大埤鄉";
            else if (checkBox109.isChecked())
                area = "莿桐鄉";
            else if (checkBox110.isChecked())
                area = "林內鄉";
            else if (checkBox111.isChecked())
                area = "二崙鄉";
            else if (checkBox112.isChecked())
                area = "崙背鄉";
            else if (checkBox113.isChecked())
                area = "麥寮鄉";
            else if (checkBox114.isChecked())
                area = "東勢鄉";
            else if (checkBox115.isChecked())
                area = "褒忠鄉";
            else if (checkBox116.isChecked())
                area = "臺西鄉";
            else if (checkBox117.isChecked())
                area = "元長鄉";
            else if (checkBox118.isChecked())
                area = "四湖鄉";
            else if (checkBox119.isChecked())
                area = "口湖鄉";
            else if (checkBox120.isChecked())
                area = "水林鄉";
        }
    };
    private CheckBox.OnCheckedChangeListener Tainancheck = new CheckBox.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            if (checkBox121.isChecked())
                area = "新營區";
            else if (checkBox122.isChecked())
                area = "新化區";
            else if (checkBox123.isChecked())
                area = "後壁區";
            else if (checkBox124.isChecked())
                area = "麻豆區";
            else if (checkBox125.isChecked())
                area = "六甲區";
            else if (checkBox126.isChecked())
                area = "官田區";
            else if (checkBox127.isChecked())
                area = "佳里區";
            else if (checkBox128.isChecked())
                area = "柳營區";
            else if (checkBox129.isChecked())
                area = "西港區";
            else if (checkBox130.isChecked())
                area = "七股區";
            else if (checkBox131.isChecked())
                area = "北門區";
            else if (checkBox132.isChecked())
                area = "安定區";
            else if (checkBox133.isChecked())
                area = "左區區";
            else if (checkBox134.isChecked())
                area = "仁德區";
            else if (checkBox135.isChecked())
                area = "永康區";
            else if (checkBox136.isChecked())
                area = "歸仁區";
            else if (checkBox137.isChecked())
                area = "鹽水區";
            else if (checkBox138.isChecked())
                area = "白河區";
            else if (checkBox139.isChecked())
                area = "東山區";
            else if (checkBox140.isChecked())
                area = "下營區";
            else if (checkBox141.isChecked())
                area = "大內區";
            else if (checkBox142.isChecked())
                area = "學甲區";
            else if (checkBox143.isChecked())
                area = "將軍區";
            else if (checkBox144.isChecked())
                area = "玉井區";
            else if (checkBox145.isChecked())
                area = "楠西區";
            else if (checkBox146.isChecked())
                area = "南化區";
            else if (checkBox147.isChecked())
                area = "善化區";
            else if (checkBox148.isChecked())
                area = "新市區";
            else if (checkBox149.isChecked())
                area = "山上區";
            else if (checkBox150.isChecked())
                area = "關廟區";
            else if (checkBox151.isChecked())
                area = "龍崎區";
            else if (checkBox152.isChecked())
                area = "北區";
            else if (checkBox153.isChecked())
                area = "東區";
            else if (checkBox154.isChecked())
                area = "中西區";
            else if (checkBox155.isChecked())
                area = "安南區";
            else if (checkBox156.isChecked())
                area = "安明區";
            else if (checkBox157.isChecked())
                area = "南區";
        }
    };
    private CheckBox.OnCheckedChangeListener Hualiencheck = new CheckBox.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            if (checkBox158.isChecked())
                area = "卓溪鄉";
            else if (checkBox159.isChecked())
                area = "萬榮鄉";
            else if (checkBox160.isChecked())
                area = "秀林鄉";
            else if (checkBox161.isChecked())
                area = "富里鄉";
            else if (checkBox162.isChecked())
                area = "瑞穗鄉";
            else if (checkBox163.isChecked())
                area = "豐濱鄉";
            else if (checkBox164.isChecked())
                area = "光復鄉";
            else if (checkBox165.isChecked())
                area = "壽豐鄉";
            else if (checkBox166.isChecked())
                area = "吉安鄉";
            else if (checkBox167.isChecked())
                area = "新城鄉";
            else if (checkBox168.isChecked())
                area = "玉里鎮";
            else if (checkBox169.isChecked())
                area = "鳳林鎮";
            else if (checkBox170.isChecked())
                area = "花蓮市";

        }
    };
    private CheckBox.OnCheckedChangeListener Miaocheck = new CheckBox.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            if (checkBox171.isChecked())
                area = "泰安鄉";
            else if (checkBox172.isChecked())
                area = "獅潭鄉";
            else if (checkBox173.isChecked())
                area = "三灣鄉";
            else if (checkBox174.isChecked())
                area = "造橋鄉";
            else if (checkBox175.isChecked())
                area = "西湖鄉";
            else if (checkBox176.isChecked())
                area = "三義鄉";
            else if (checkBox177.isChecked())
                area = "頭屋鄉";
            else if (checkBox178.isChecked())
                area = "南庄鄉";
            else if (checkBox179.isChecked())
                area = "銅鑼鄉";
            else if (checkBox180.isChecked())
                area = "公館鄉";
            else if (checkBox181.isChecked())
                area = "大湖鄉";
            else if (checkBox182.isChecked())
                area = "卓蘭鎮";
            else if (checkBox183.isChecked())
                area = "後龍鎮";
            else if (checkBox184.isChecked())
                area = "頭份市";
            else if (checkBox185.isChecked())
                area = "竹南鎮";
            else if (checkBox186.isChecked())
                area = "通霄鎮";
            else if (checkBox187.isChecked())
                area = "苑裡鎮";
            else if (checkBox188.isChecked())
                area = "苗栗市";

        }
    };
    private CheckBox.OnCheckedChangeListener Taidongcheck = new CheckBox.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            if (checkBox189.isChecked())
                area = "台東市";
            else if (checkBox190.isChecked())
                area = "卑南鄉";
            else if (checkBox191.isChecked())
                area = "大武鄉";
            else if (checkBox192.isChecked())
                area = "成功鎮";
            else if (checkBox193.isChecked())
                area = "關山鎮";
            else if (checkBox194.isChecked())
                area = "鹿野鄉";
            else if (checkBox195.isChecked())
                area = "池上鄉";
            else if (checkBox196.isChecked())
                area = "東河鄉";
            else if (checkBox197.isChecked())
                area = "長濱鄉";
            else if (checkBox198.isChecked())
                area = "太麻里鄉";
            else if (checkBox199.isChecked())
                area = "綠島鄉";
            else if (checkBox200.isChecked())
                area = "海端鄉";
            else if (checkBox201.isChecked())
                area = "延平鄉";
            else if (checkBox202.isChecked())
                area = "金峰鄉";
            else if (checkBox203.isChecked())
                area = "達仁鄉";
            else if (checkBox204.isChecked())
                area = "蘭嶼鄉";
        }
    };
    private CheckBox.OnCheckedChangeListener Pindongcheck = new CheckBox.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            if (checkBox205.isChecked())
                area = "內埔鄉";
            else if (checkBox206.isChecked())
                area = "麟洛鄉";
            else if (checkBox207.isChecked())
                area = "屏東市";
            else if (checkBox208.isChecked())
                area = "林邊鄉";
            else if (checkBox209.isChecked())
                area = "牡丹鄉";
            else if (checkBox210.isChecked())
                area = "春日鄉";
            else if (checkBox211.isChecked())
                area = "來義鄉";
            else if (checkBox212.isChecked())
                area = "泰武鄉";
            else if (checkBox213.isChecked())
                area = "瑪家鄉";
            else if (checkBox214.isChecked())
                area = "霧臺鄉";
            else if (checkBox215.isChecked())
                area = "三地門鄉";
            else if (checkBox216.isChecked())
                area = "枋山鄉";
            else if (checkBox217.isChecked())
                area = "滿州鄉";
            else if (checkBox218.isChecked())
                area = "車城鄉";
            else if (checkBox219.isChecked())
                area = "琉球鄉";
            else if (checkBox220.isChecked())
                area = "佳冬鄉";
            else if (checkBox221.isChecked())
                area = "南州鄉";
            else if (checkBox222.isChecked())
                area = "崁頂鄉";
            else if (checkBox223.isChecked())
                area = "新園鄉";
            else if (checkBox224.isChecked())
                area = "枋寮鄉";
            else if (checkBox225.isChecked())
                area = "新埤鄉";
            else if (checkBox226.isChecked())
                area = "竹田鄉";
            else if (checkBox227.isChecked())
                area = "萬巒鄉";
            else if (checkBox228.isChecked())
                area = "高樹鄉";
            else if (checkBox229.isChecked())
                area = "長治鄉";
            else if (checkBox230.isChecked())
                area = "里港鄉";
            else if (checkBox231.isChecked())
                area = "九如鄉";
            else if (checkBox232.isChecked())
                area = "鹽埔鄉";
            else if (checkBox233.isChecked())
                area = "萬丹鄉";
            else if (checkBox234.isChecked())
                area = "恆春鄉";
            else if (checkBox235.isChecked())
                area = "東港鄉";
            else if (checkBox236.isChecked())
                area = "潮州鄉";
        }
    };
    private CheckBox.OnCheckedChangeListener Shinchucountcheck = new CheckBox.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            if (checkBox237.isChecked())
                area = "五峰鄉";
            else if (checkBox238.isChecked())
                area = "尖石鄉";
            else if (checkBox239.isChecked())
                area = "峨嵋市";
            else if (checkBox240.isChecked())
                area = "寶山鄉";
            else if (checkBox241.isChecked())
                area = "北埔鄉";
            else if (checkBox242.isChecked())
                area = "橫山鄉";
            else if (checkBox243.isChecked())
                area = "芎林鄉";
            else if (checkBox244.isChecked())
                area = "新豐鄉";
            else if (checkBox245.isChecked())
                area = "湖口鄉";
            else if (checkBox246.isChecked())
                area = "關西鎮";
            else if (checkBox247.isChecked())
                area = "新埔鎮";
            else if (checkBox248.isChecked())
                area = "竹東鎮";
            else if (checkBox249.isChecked())
                area = "竹北市";

        }
    };
    private CheckBox.OnCheckedChangeListener Chunghuacheck = new CheckBox.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            if (checkBox250.isChecked())
                area = "員林市";
            else if (checkBox251.isChecked())
                area = "秀水鄉";
            else if (checkBox252.isChecked())
                area = "溪州鄉";
            else if (checkBox253.isChecked())
                area = "竹塘鄉";
            else if (checkBox254.isChecked())
                area = "大城鄉";
            else if (checkBox255.isChecked())
                area = "芳苑鄉";
            else if (checkBox256.isChecked())
                area = "埤頭鄉";
            else if (checkBox257.isChecked())
                area = "田尾鄉";
            else if (checkBox258.isChecked())
                area = "二林鎮";
            else if (checkBox259.isChecked())
                area = "北斗鎮";
            else if (checkBox260.isChecked())
                area = "二水鄉";
            else if (checkBox261.isChecked())
                area = "社頭鄉";
            else if (checkBox262.isChecked())
                area = "永靖鄉";
            else if (checkBox263.isChecked())
                area = "埔心鄉";
            else if (checkBox264.isChecked())
                area = "彰化市";
            else if (checkBox265.isChecked())
                area = "埔鹽鄉";
            else if (checkBox266.isChecked())
                area = "大村鄉";
            else if (checkBox267.isChecked())
                area = "田中鎮";
            else if (checkBox268.isChecked())
                area = "溪湖鎮";
            else if (checkBox269.isChecked())
                area = "芬園鄉";
            else if (checkBox270.isChecked())
                area = "花壇鄉";
            else if (checkBox271.isChecked())
                area = "福興鄉";
            else if (checkBox272.isChecked())
                area = "伸港鄉";
            else if (checkBox273.isChecked())
                area = "線西鄉";
            else if (checkBox274.isChecked())
                area = "和美鎮";

        }
    };
    private CheckBox.OnCheckedChangeListener Shinchucheck = new CheckBox.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            if (checkBox275.isChecked())
                area = "香山區";
            else if (checkBox276.isChecked())
                area = "北區";
            else if (checkBox277.isChecked())
                area = "東區";

        }
    };
    private CheckBox.OnCheckedChangeListener Nantoucheck = new CheckBox.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            if (checkBox278.isChecked())
                area = "信義鄉";
            else if (checkBox279.isChecked())
                area = "水里鄉";
            else if (checkBox280.isChecked())
                area = "國姓鄉";
            else if (checkBox281.isChecked())
                area = "魚池鄉";
            else if (checkBox282.isChecked())
                area = "中寮鄉";
            else if (checkBox283.isChecked())
                area = "鹿谷鄉";
            else if (checkBox284.isChecked())
                area = "名間鄉";
            else if (checkBox285.isChecked())
                area = "集集鎮";
            else if (checkBox286.isChecked())
                area = "竹山鎮";
            else if (checkBox287.isChecked())
                area = "草屯鎮";
            else if (checkBox288.isChecked())
                area = "埔里鎮";
            else if (checkBox289.isChecked())
                area = "南投市";
            else if (checkBox290.isChecked())
                area = "仁愛鄉";

        }
    };
    //切換到介面2開始查詢
    private Button.OnClickListener btnClickListener = new Button.OnClickListener(){
        @Override
        public void onClick(View view) {
            if(city!=null && area!=null) {
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, Main2Activity.class);

                Bundle bundle = new Bundle();
                bundle.putString("city",city);
                bundle.putString("area",area);

                intent.putExtras(bundle);
                //Log.e("key",city+" "+area);

                startActivity(intent);
                Toast.makeText(getApplicationContext(),"擷取資料中...",Toast.LENGTH_SHORT).show();
            }
            else{
                Toast.makeText(getApplicationContext(),"你還沒選取市區!",Toast.LENGTH_SHORT).show();
            }
        }
    };
}


