import ddf.minim.*;
AudioPlayer music;
Minim minim;
PImage red,blue,bomb,explosion,start,bluewin,redwin,red_die,blue_die,obstacle,flower;
int redX=900,redY=900,blueX=0,blueY=0,deadtime1=0,deadtime2=0;
int[] bombArr = new int[100];
float[] bombX = new float[100];
float[] bombY = new float[100];
float[] bombS = new float[100];
int i=0,j=4,m,n,r=0;
boolean[][] position = new boolean[16][16];
boolean[][] flowers = new boolean[17][17];
boolean[] bombShow = new boolean[100];
boolean[] explosionShow = new boolean[100];
boolean gameShow,redDied,blueDied;
float[] time = new float[100];

void setup()
{
  size(960,960);
  background(255);
  minim = new Minim(this);
  gameShow = true;
  redDied = false;
  blueDied = false;
  for(n=0;n<8;n++){
    bombX[n]=-100;
    bombY[n]=-100;
    bombS[n]=40;
    time[n]=0;
    bombShow[n]=false;
    explosionShow[n]=false;
  }
  for(n=0;n<16;n++){
    for(m=0;m<16;m++)
      position[n][m]=false;
  }
  
  red = loadImage("sleeper.png");
  bomb = loadImage("bomb.png");
  explosion = loadImage("explosion.png");
  start = loadImage("start.png");
  blue = loadImage("blue.png");
  bluewin = loadImage("bluewin.jpg");
  redwin = loadImage("redwin.jpg");
  blue_die = loadImage("blue_die.png");
  red_die = loadImage("red_die.png");
  obstacle = loadImage("obstacle.png");
  flower = loadImage("flower.png");
  music = minim.loadFile("music.mp3");
  
  flowers[0][10]=true;
  flowers[1][10]=true;
  flowers[2][9]=true;
  flowers[2][10]=true;
  flowers[3][2]=true;
  flowers[3][3]=true;
  flowers[4][3]=true;
  flowers[4][6]=true;
  flowers[4][9]=true;
  flowers[5][3]=true;
  flowers[5][6]=true;
  flowers[6][4]=true;
  flowers[6][5]=true;
  flowers[6][6]=true;
  flowers[7][7]=true;
  flowers[7][8]=true;
  flowers[7][9]=true;
  flowers[7][10]=true;
  flowers[7][11]=true;
  flowers[10][7]=true;
  flowers[10][10]=true;
  flowers[10][11]=true;
  flowers[11][6]=true;
  flowers[11][7]=true;
  flowers[11][15]=true;
  flowers[12][15]=true;
  flowers[13][11]=true;
  flowers[13][12]=true;
  flowers[14][3]=true;
  flowers[14][7]=true;
  flowers[14][8]=true;
  flowers[14][11]=true;
  flowers[14][12]=true;
  flowers[15][3]=true;
}

void draw()
{
  if(gameShow == true)
  {
    image(start,0,0,960,960);
    fill(200,200,0);
    rect(470,30,500,100);
    textSize(60);
    fill(0,0,0);
    text("Go Fighting ! !", 500, 100); 
  }
  if(mousePressed == true && mouseX <= 950 && mouseX >= 500 
  && mouseY <= 120 && mouseY >= 45)
  {
    gameShow = false;

  }
  if(gameShow == false)
  {
    background(150,255,150);
   //music.play();
    for(int k=1;k<16;k++)
    {
      noStroke();
      fill(0,150,0);
      rect(60*k,0,2,960);
      fill(0,150,0);
      rect(0,60*k,960,2);
    }
    //obstacles
    position[0][2]=true; image(obstacle,0,120,60,60);
    position[0][3]=true; image(obstacle,0,180,60,60);
    position[0][15]=true; image(obstacle,0,900,60,60);
    position[1][2]=true; image(obstacle,60,120,60,60);
    position[1][3]=true; image(obstacle,60,180,60,60);
    position[1][5]=true; image(obstacle,60,300,60,60);
    position[1][6]=true; image(obstacle,60,360,60,60);
    position[1][11]=true; image(obstacle,60,660,60,60);
    position[1][12]=true; image(obstacle,60,720,60,60);
    position[1][13]=true; image(obstacle,60,780,60,60);
    position[2][2]=true; image(obstacle,120,120,60,60);
    position[2][3]=true; image(obstacle,120,180,60,60);
    position[2][8]=true; image(obstacle,120,480,60,60);
    position[2][11]=true; image(obstacle,120,660,60,60);
    position[2][12]=true; image(obstacle,120,720,60,60);
    position[2][13]=true; image(obstacle,120,780,60,60);
    position[3][7]=true; image(obstacle,180,420,60,60);
    position[3][8]=true; image(obstacle,180,480,60,60);
    position[3][9]=true; image(obstacle,180,540,60,60);
    position[3][11]=true; image(obstacle,180,660,60,60);
    position[3][12]=true; image(obstacle,180,720,60,60);
    position[3][13]=true; image(obstacle,180,780,60,60);
    position[4][4]=true; image(obstacle,240,240,60,60);
    position[4][5]=true; image(obstacle,240,300,60,60);
    position[4][7]=true; image(obstacle,240,420,60,60);
    position[4][8]=true; image(obstacle,240,480,60,60);
    position[4][15]=true; image(obstacle,240,900,60,60);
    position[5][0]=true; image(obstacle,300,0,60,60);
    position[5][1]=true; image(obstacle,300,60,60,60);
    position[5][15]=true; image(obstacle,300,900,60,60);
    position[6][1]=true; image(obstacle,360,60,60,60);
    position[6][2]=true; image(obstacle,360,120,60,60);
    position[6][3]=true; image(obstacle,360,180,60,60);
    position[6][12]=true; image(obstacle,360,720,60,60);
    position[6][13]=true; image(obstacle,360,780,60,60);
    position[6][14]=true; image(obstacle,360,840,60,60);
    position[6][15]=true; image(obstacle,360,900,60,60);
    position[7][6]=true; image(obstacle,420,360,60,60);
    position[7][12]=true; image(obstacle,420,720,60,60);
    position[7][13]=true; image(obstacle,420,780,60,60);
    position[7][14]=true; image(obstacle,420,840,60,60);
    position[7][15]=true; image(obstacle,420,900,60,60);
    position[8][3]=true; image(obstacle,480,180,60,60);
    position[8][5]=true; image(obstacle,480,300,60,60);
    position[8][6]=true; image(obstacle,480,360,60,60);
    position[8][7]=true; image(obstacle,480,420,60,60);
    position[8][9]=true; image(obstacle,480,540,60,60);
    position[9][0]=true; image(obstacle,540,0,60,60);
    position[9][1]=true; image(obstacle,540,60,60,60);
    position[9][5]=true; image(obstacle,540,300,60,60);
    position[9][6]=true; image(obstacle,540,360,60,60);
    position[9][7]=true; image(obstacle,540,420,60,60);
    position[9][9]=true; image(obstacle,540,540,60,60);
    position[10][0]=true; image(obstacle,600,0,60,60);
    position[10][1]=true; image(obstacle,600,60,60,60);
    position[10][2]=true; image(obstacle,600,120,60,60);
    position[10][6]=true; image(obstacle,600,360,60,60);
    position[10][9]=true; image(obstacle,600,540,60,60);
    position[10][12]=true; image(obstacle,600,720,60,60);
    position[10][13]=true; image(obstacle,600,780,60,60);
    position[10][14]=true; image(obstacle,600,840,60,60);
    position[10][15]=true; image(obstacle,600,900,60,60);
    position[11][0]=true; image(obstacle,660,0,60,60);
    position[11][1]=true; image(obstacle,660,60,60,60);
    position[11][9]=true; image(obstacle,660,540,60,60);
    position[12][4]=true; image(obstacle,720,240,60,60);
    position[12][5]=true; image(obstacle,720,300,60,60);
    position[12][6]=true; image(obstacle,720,360,60,60);
    position[12][7]=true; image(obstacle,720,420,60,60);
    position[12][9]=true; image(obstacle,720,540,60,60);
    position[13][4]=true; image(obstacle,780,240,60,60);
    position[13][5]=true; image(obstacle,780,300,60,60);
    position[13][6]=true; image(obstacle,780,360,60,60);
    position[13][7]=true; image(obstacle,780,420,60,60);
    position[13][13]=true; image(obstacle,780,780,60,60);
    position[13][14]=true; image(obstacle,780,840,60,60);
    position[14][0]=true; image(obstacle,840,0,60,60);
    position[14][1]=true; image(obstacle,840,60,60,60);
    position[14][2]=true; image(obstacle,840,120,60,60);
    position[14][14]=true; image(obstacle,840,840,60,60);
    position[15][0]=true; image(obstacle,900,0,60,60);
    position[15][1]=true; image(obstacle,900,60,60,60);
    position[15][2]=true; image(obstacle,900,120,60,60);
    position[15][7]=true; image(obstacle,900,420,60,60);
    position[15][8]=true; image(obstacle,900,480,60,60);
    position[15][9]=true; image(obstacle,900,540,60,60);
    position[15][10]=true; image(obstacle,900,600,60,60);
    position[15][11]=true; image(obstacle,900,660,60,60);
    //flowers
    if(flowers[0][10]==true){position[0][10]=true; image(flower,0,600,60,60);}else  position[0][10]=false;
    if(flowers[1][10]==true){position[1][10]=true; image(flower,60,600,60,60);}else position[1][10]=false;
    if(flowers[2][9]==true){position[2][9]=true; image(flower,120,540,60,60);}else position[2][9]=false;
    if(flowers[2][10]==true){position[2][10]=true; image(flower,120,600,60,60);}else position[2][10]=false;
    if(flowers[3][2]==true){position[3][2]=true; image(flower,180,120,60,60);}else position[3][2]=false;
    if(flowers[3][3]==true){position[3][3]=true; image(flower,180,180,60,60);}else position[3][3]=false;
    if(flowers[4][3]==true){position[4][3]=true; image(flower,240,180,60,60);}else position[4][3]=false;
    if(flowers[4][6]==true){position[4][6]=true; image(flower,240,360,60,60);}else position[4][6]=false;
    if(flowers[4][9]==true){position[4][9]=true; image(flower,240,540,60,60);}else position[4][9]=false;
    if(flowers[5][3]==true){position[5][3]=true; image(flower,300,180,60,60);}else position[5][3]=false;
    if(flowers[5][6]==true){position[5][6]=true; image(flower,300,360,60,60);}else position[5][6]=false;
    if(flowers[6][4]==true){position[6][4]=true; image(flower,360,240,60,60);}else position[6][4]=false;
    if(flowers[6][5]==true){position[6][5]=true; image(flower,360,300,60,60);}else position[6][5]=false;
    if(flowers[6][6]==true){position[6][6]=true; image(flower,360,360,60,60);}else position[6][6]=false;
    if(flowers[7][7]==true){position[7][7]=true; image(flower,420,420,60,60);}else position[7][7]=false;
    if(flowers[7][8]==true){position[7][8]=true; image(flower,420,480,60,60);}else position[7][8]=false;
    if(flowers[7][9]==true){position[7][9]=true; image(flower,420,540,60,60);}else position[7][9]=false;
    if(flowers[7][10]==true){position[7][10]=true; image(flower,420,600,60,60);}else position[7][10]=false;
    if(flowers[7][11]==true){position[7][11]=true; image(flower,420,660,60,60);}else position[7][11]=false;
    if(flowers[10][7]==true){position[10][7]=true; image(flower,600,420,60,60);}else position[10][7]=false;
    if(flowers[10][10]==true){position[10][10]=true; image(flower,600,600,60,60);}else position[10][10]=false;
    if(flowers[10][11]==true){position[10][11]=true; image(flower,600,660,60,60);}else position[10][11]=false;
    if(flowers[11][6]==true){position[11][6]=true; image(flower,660,360,60,60);}else position[11][6]=false;
    if(flowers[11][7]==true){position[11][7]=true; image(flower,660,420,60,60);}else position[11][7]=false;
    if(flowers[11][15]==true){position[11][15]=true; image(flower,660,900,60,60);}else position[11][15]=false;
    if(flowers[12][15]==true){position[12][15]=true; image(flower,720,900,60,60);}else position[12][15]=false;
    if(flowers[13][11]==true){position[13][11]=true; image(flower,780,660,60,60);}else position[13][11]=false;
    if(flowers[13][12]==true){position[13][12]=true; image(flower,780,720,60,60);}else position[13][12]=false;
    if(flowers[14][3]==true){position[14][3]=true; image(flower,840,180,60,60);}else position[14][3]=false;
    if(flowers[14][7]==true){position[14][7]=true; image(flower,840,420,60,60);}else position[14][7]=false;
    if(flowers[14][8]==true){position[14][8]=true; image(flower,840,480,60,60);}else position[14][8]=false;
    if(flowers[14][11]==true){position[14][11]=true; image(flower,840,660,60,60);}else position[14][11]=false;
    if(flowers[14][12]==true){position[14][12]=true; image(flower,840,720,60,60);}else position[14][12]=false;
    if(flowers[15][3]==true){position[15][3]=true; image(flower,900,180,60,60);}else position[15][3]=false;
    for(m=0;m<8;m++)
    {
      if(bombShow[m]==true)
      {
        image(bomb,bombX[m]+10,bombY[m]+10,bombS[m],bombS[m]);
        bombX[m]-=0.1;
        bombY[m]-=0.1;
        bombS[m]+=0.2;
      }
      if(bombS[m]>70)
      {
        explosionShow[m]=true;
        if(redX>bombX[m]-60 && redX<bombX[m]+120 && redY>bombY[m]-60 &&
        redY<bombY[m]+120)redDied=true;
        if(blueX>bombX[m]-60 && blueX<bombX[m]+120 && blueY>bombY[m]-60 &&
        blueY<bombY[m]+120)blueDied=true;
        bombS[m]=40;
        bombShow[m]=false;
        position[(int)bombX[m]/60+1][(int)bombY[m]/60+1]=false;
      }
      if(explosionShow[m]==true)
      {
        image(explosion,bombX[m]-60,bombY[m]-60,240,240);
        flowers[(int)bombX[m]/60][(int)bombY[m]/60]=false;
        flowers[(int)bombX[m]/60][(int)bombY[m]/60+1]=false;
        flowers[(int)bombX[m]/60][(int)bombY[m]/60+2]=false;
        flowers[(int)bombX[m]/60+1][(int)bombY[m]/60]=false;
        flowers[(int)bombX[m]/60+1][(int)bombY[m]/60+2]=false;
        flowers[(int)bombX[m]/60+2][(int)bombY[m]/60]=false;
        flowers[(int)bombX[m]/60+2][(int)bombY[m]/60+1]=false;
        flowers[(int)bombX[m]/60+2][(int)bombY[m]/60+2]=false;
        
        time[m]+=1;
      }
      if(time[m]>10)
      {
        explosionShow[m]=false;
        time[m]=0;
        
      }
    }
   
  image(red,redX+2,redY+2,60,60);
  image(blue,blueX-2,blueY,70,70);
  
  if(redDied==true){
    image(red_die,redX+4,redY+2,60,60);
    if(deadtime2<100)deadtime1++;
    if(deadtime1>=100 && deadtime2<100)image(bluewin,0,0,960,960);
  }
  if(blueDied==true){
    image(blue_die,blueX-2,blueY,70,70);
    if(deadtime1<100)deadtime2++;
    if(deadtime2>=100 && deadtime1<100)image(redwin,0,0,960,960);
  }
}
}

void keyPressed()
{
  if(keyCode==UP && redY>0 && position[redX/60][redY/60-1]==false && redDied==false)redY-=60;
  if(keyCode==DOWN && redY<900 && position[redX/60][redY/60+1]==false && redDied==false)redY+=60;
  if(keyCode==LEFT && redX>0 && position[redX/60-1][redY/60]==false && redDied==false)redX-=60; 
  if(keyCode==RIGHT && redX<900 && position[redX/60+1][redY/60]==false && redDied==false)redX+=60;

  if(key=='w' && blueY>0 && position[blueX/60][blueY/60-1]==false && blueDied==false)blueY-=60;
  if(key=='s' && blueY<900 && position[blueX/60][blueY/60+1]==false && blueDied==false)blueY+=60;
  if(key=='a' && blueX>0 && position[blueX/60-1][blueY/60]==false && blueDied==false)blueX-=60; 
  if(key=='d' && blueX<900 && position[blueX/60+1][blueY/60]==false && blueDied==false)blueX+=60;
  
  if(keyCode==ENTER)
  {
    if(bombShow[i]==false && explosionShow[i]==false &&
    position[redX/60][redY/60]==false){
      position[redX/60][redY/60]=true;
      bombShow[i]=true;
      bombX[i]=redX;
      bombY[i]=redY;
      i++;
    }
    
    if(i>3)i=0;
  }
  
  if(key==' ')
  {
    if(bombShow[j]==false && explosionShow[j]==false &&
    position[blueX/60][blueY/60]==false){
      position[blueX/60][blueY/60]=true;
      bombShow[j]=true;
      bombX[j]=blueX;
      bombY[j]=blueY;
      j++;
    }
    
    if(j>7)j=4;
  }

}
