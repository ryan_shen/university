void setup() {
size(800, 600);
background(255,255,255);
strokeWeight(5);
smooth();
fill(255,0,0);
rect(0,0,80,80);
fill(0,255,0);
rect(0,80,80,80);
fill(0,0,255);
rect(0,160,80,80);
fill(255,255,0);
rect(0,240,80,80);
fill(0,0,0);
rect(0,320,80,80);

fill(255,255,255);
rect(720,0,80,80);
fill(255,255,255);
rect(720,80,80,80);
fill(255,255,255);
rect(720,160,80,80);
fill(255,255,255);
rect(720,240,80,80);
fill(255,255,255);
rect(720,320,80,80);

fill(0,0,0);
text("eraser",745,40);
fill(0,0,0);
text("save",745,120);
fill(0,0,0);
ellipse(760,200,5,5);
fill(0,0,0);
ellipse(760,280,10,10);
fill(0,0,0);
text("clear",745,360);
}
void draw() {
if (mouseX<=80 && mouseY<=80 && mouseButton == LEFT){
    stroke(255,0,0);
}
if (mouseX<=80 && mouseY>=80 && mouseY<=160 
&& mouseButton == LEFT){
    stroke(0,255,0);
}
if (mouseX<=80 && mouseY>=160 && mouseY<=240 
&& mouseButton == LEFT){
    stroke(0,0,255);
}
if (mouseX<=80 && mouseY>=240 && mouseY<=320 
&& mouseButton == LEFT){
    stroke(255,255,0);
}
if (mouseX<=80 && mouseY>=320 && mouseY<=400 
&& mouseButton == LEFT){
    stroke(0,0,0);
}
else if(mouseButton == LEFT){
 line(mouseX,mouseY,pmouseX,pmouseY);
}

if (mouseX>=720 && mouseY<=80 && mouseButton == LEFT){
    stroke(255,255,255);
}
if (mouseX>=720 && mouseY>=80 && mouseY<=160 
&& mouseButton == LEFT){
  save("paint");
}
if (mouseX>=720 && mouseY>=160 && mouseY<=240 
&& mouseButton == LEFT){
  strokeWeight(5);
}
if (mouseX>=720 && mouseY>=240 && mouseY<=320 
&& mouseButton == LEFT){
  strokeWeight(10);
}
if (mouseX>=720 && mouseY>=320 && mouseY<=400 
&& mouseButton == LEFT){
    background(255,255,255);
    stroke(0,0,0);
    setup();
}
}
