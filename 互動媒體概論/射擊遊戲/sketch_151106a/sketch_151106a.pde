int n=0,fly,bulletX,bulletY;
int i,appear=0,run;
int[] monsterX = new int[3];
int[] monsterY = new int[3];
boolean[] monster = new boolean[3];

void setup(){
  size(600,800);
  monster[0]=false;
  monster[1]=false;
  monster[2]=false;
  for(i=0;i<3;i++){
    monsterX[i]=(int)random(0,550);
    monsterY[i]=0;
  }
  i=0;
}

void draw(){

  background(200);
  fill(0,255,0);
  rect(mouseX-15,mouseY-25,30,10);
  rect(mouseX-25,mouseY-15,50,30);
  
  ///Score
  fill(0);
  textSize(30);
  text("Score: "+n,100,100);
  
  ///Bullet
  if(mousePressed==true){
    fly=0;
    bulletX=mouseX;
    bulletY=mouseY;
  }
  ellipse(bulletX,bulletY-30-fly,10,10);
  fly++; 
  
  ///Create monster
  for(run=0;run<3;run++){
    if(monster[run]==true){
      fill(200,100,0);
      rect(monsterX[run],monsterY[run],50,50);
      fill(0);
      ellipse(monsterX[run]+15,monsterY[run]+15,10,10);
      ellipse(monsterX[run]+35,monsterY[run]+15,10,10);
      fill(255,0,0);
      rect(monsterX[run]+15,monsterY[run]+30,20,10);
      monsterY[run]++;
    }
  }
  
  ///Delete and score
  for(i=0;i<3;i++){
  if(bulletX>=monsterX[i] && bulletX<=monsterX[i]+50 &&
  bulletY-30-fly<=monsterY[i]+50){
   monster[i]=false; 
   bulletX=1000;
   bulletY=1000;
   n++;
  }
  }
  
  ///Reset monster
  if(monster[appear]==false){
    monsterX[appear]=(int)random(0,550);
    monsterY[appear]=0;
    monster[appear]=true;
   }
   appear++;
  if(appear>2)appear=0;
}


