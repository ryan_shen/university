﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="web.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            color: #FF0000;
            text-align: center;
        }
        .auto-style3 {
            width: 619px;
        }
        .auto-style4 {
            width: 48px;
        }
    </style>
</head>
<body style="background-image: url('img_6861_1920.jpg')">
    <form id="form1" runat="server">
        <div class="auto-style2">
            <strong>歡迎來到飲茶店</strong></div>
        <table class="auto-style1">
            <tr>
                <td class="auto-style3">&nbsp;</td>
                <td class="auto-style4">
                    <asp:Label ID="Label1" runat="server" Text="帳號:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">&nbsp;</td>
                <td class="auto-style4">
                    <asp:Label ID="Label2" runat="server" Text="密碼:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">&nbsp;</td>
                <td class="auto-style4">
                    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="登入" />
                </td>
                <td>
                    <asp:LinkButton ID="LinkButton1" runat="server" Visible="False" PostBackUrl="~/store.aspx" OnClick="LinkButton1_Click">進入商店</asp:LinkButton>
                </td>
            </tr>
        </table>
        <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" DataSourceID="SqlDataSource1" Height="50px" Visible="False" Width="125px">
            <EmptyDataTemplate>
                帳號密碼錯誤
            </EmptyDataTemplate>
            <Fields>
                <asp:BoundField DataField="name" HeaderText="name" SortExpression="name" />
                <asp:BoundField DataField="money" HeaderText="money" SortExpression="money" />
            </Fields>
        </asp:DetailsView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" OnSelecting="SqlDataSource1_Selecting" SelectCommand="SELECT [name], [money] FROM [user1] WHERE (([name] = @name) AND ([phone] = @phone))">
            <SelectParameters>
                <asp:ControlParameter ControlID="TextBox1" Name="name" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="TextBox2" Name="phone" PropertyName="Text" Type="String" />
            </SelectParameters>
        </asp:SqlDataSource>
    </form>
</body>
</html>
